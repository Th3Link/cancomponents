#include <chrono>
#include <ctime>
#include <message/Atomic.hpp>
#include "stm32f1xx_hal.h"
#include <sys/time.h>

extern "C" int gettimeofday(struct timeval* tv, void* tz) {
    auto tick = HAL_GetTick();
    tv->tv_sec  = tick/1000;
    tv->tv_usec = (tick%1000)*1000;
    return 0;
}

void message::atomic(bool lock)
{
    if (lock)
    {
        //__disable_irq();
    }
    else
    {
        //__enable_irq();
    }
}
