//std
#include <algorithm>

//lib

//local
#include <lowLevel/CAN.hpp>
#include <stm32f1xx_hal.h>

//suppress name mengling for callback functions
extern "C"
{
    void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef* hcan);
    void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef* hcan);
    void HAL_CAN_RxFifo0MsgFullCallback(CAN_HandleTypeDef* hcan);
    void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan);
}

extern CAN_HandleTypeDef hcan;

using namespace lowLevel;

void lowLevel::CAN::init(uint8_t baudrate)
{
    hcan.Instance = CAN1;
    hcan.Init.Prescaler = 20;
    hcan.Init.Mode = CAN_MODE_NORMAL;
    hcan.Init.SyncJumpWidth = CAN_SJW_1TQ;
    hcan.Init.TimeSeg1 = CAN_BS1_5TQ;
    hcan.Init.TimeSeg2 = CAN_BS2_2TQ;
    hcan.Init.TimeTriggeredMode = DISABLE;
    hcan.Init.AutoBusOff = DISABLE;
    hcan.Init.AutoWakeUp = DISABLE;
    hcan.Init.AutoRetransmission = ENABLE;
    hcan.Init.ReceiveFifoLocked = DISABLE;
    hcan.Init.TransmitFifoPriority = DISABLE;
    
    if (baudrate == lowLevel::CAN::BAUDRATE_SETTING::BAUDRATE_22_222KBITS)
    {
        hcan.Init.TimeSeg1 = CAN_BS1_10TQ;
        hcan.Init.TimeSeg2 = CAN_BS2_7TQ;
    }
    else if (baudrate == lowLevel::CAN::BAUDRATE_SETTING::BAUDRATE_25KBITS)
    {
        hcan.Init.TimeSeg1 = CAN_BS1_9TQ;
        hcan.Init.TimeSeg2 = CAN_BS2_6TQ;
    }
    else if (baudrate == lowLevel::CAN::BAUDRATE_SETTING::BAUDRATE_50KBITS)
    {
        hcan.Init.TimeSeg1 = CAN_BS1_5TQ;
        hcan.Init.TimeSeg2 = CAN_BS2_2TQ;
    }
    else if (baudrate == lowLevel::CAN::BAUDRATE_SETTING::BAUDRATE_100KBITS)
    {
        hcan.Init.TimeSeg1 = CAN_BS1_3TQ;
        hcan.Init.TimeSeg2 = CAN_BS2_4TQ;
    }
    
    // no error handling here. When the bootloader fails... to bad. when the aplication
    // fails it gets reset into bootloader by the watchdog
    HAL_CAN_Init(&hcan);
    
    can_id_t id;
    id.id = 0;
    id.ng_bit = 1;
    
    can_id_t mask;
    mask.id = 0;
    mask.ng_bit = 1;
    mask.device_id = 0xFF;
    mask.type = 0x3F;
    mask.msg_id = 0xF8;
    
    // allow availability broadcast (msgid = 0-7)
    CAN_FilterTypeDef myFilter;
    myFilter.FilterBank              = 0;
	myFilter.FilterMode             = CAN_FILTERMODE_IDMASK;
	myFilter.FilterScale            = CAN_FILTERSCALE_32BIT;
	myFilter.FilterIdHigh           = id.id >> 16;
	myFilter.FilterIdLow            = (id.id & 0x0000FFFF);
	myFilter.FilterFIFOAssignment   = 0;
	myFilter.FilterActivation       = ENABLE;
	myFilter.FilterMaskIdHigh = mask.id >> 16;
	myFilter.FilterMaskIdLow = (mask.id & 0x0000FFFF);
	HAL_CAN_ConfigFilter(&hcan,&myFilter);
    
    // Message Pending Interrupt for FIFO0
    HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO0_MSG_PENDING);
    HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO1_MSG_PENDING);
    
    HAL_CAN_Start(&hcan);
    
}

void lowLevel::CAN::setFilter(const uint8_t device_id, const uint8_t device_type, const uint8_t group[6])
{
    // allow direct device addressing
    if (device_id && device_type)
    {
        can_id_t id;
        id.id = 0;
        id.ng_bit = 1;
        id.device_id = device_id;
        id.type = device_type;
        
        can_id_t mask;
        mask.id = 0;
        mask.ng_bit = 1;
        mask.device_id = 0xFF;
        mask.type = 0x3F;
        
        //Broadcast
        CAN_FilterTypeDef myFilter;
        myFilter.FilterBank              = 1;
        myFilter.FilterMode             = CAN_FILTERMODE_IDMASK;
        myFilter.FilterScale            = CAN_FILTERSCALE_32BIT;
        myFilter.FilterIdHigh           = id.id >> 16;
        myFilter.FilterIdLow            = (id.id & 0x0000FFFF)  | 0x04;
        myFilter.FilterFIFOAssignment   = 0;
        myFilter.FilterActivation       = ENABLE;
        myFilter.FilterMaskIdHigh = mask.id >> 16;
        myFilter.FilterMaskIdLow = (mask.id & 0x0000FFFF) | 0x04;
        HAL_CAN_ConfigFilter(&hcan,&myFilter);
    }
    
    for (unsigned i = 0; i < 6; i++)
    {
        uint32_t bank = 2;
        if (!group[i] && device_type)
        {
            continue;
        }
        
        can_id_t id;
        id.id = 0;
        id.ng_bit = 1;
        id.group = group[i];
        id.type = device_type;
        
        can_id_t mask;
        mask.id = 0;
        mask.ng_bit = 1;
        mask.group = 0x3F;
        mask.type = 0x3F;
        
        //Broadcast
        CAN_FilterTypeDef myFilter;
        myFilter.FilterBank              = bank;
        myFilter.FilterMode             = CAN_FILTERMODE_IDMASK;
        myFilter.FilterScale            = CAN_FILTERSCALE_32BIT;
        myFilter.FilterIdHigh           = id.id >> 16;
        myFilter.FilterIdLow            = (id.id & 0x0000FFFF)  | 0x04;
        myFilter.FilterFIFOAssignment   = 0;
        myFilter.FilterActivation       = ENABLE;
        myFilter.FilterMaskIdHigh = mask.id >> 16;
        myFilter.FilterMaskIdLow = (mask.id & 0x0000FFFF) | 0x04;
        HAL_CAN_ConfigFilter(&hcan,&myFilter);
        bank++;
    }
}

void lowLevel::CAN::send(uint32_t id, CAN::data_t& data)
{
    uint8_t datatx[8];
    
    CAN_TxHeaderTypeDef header;
    
    header.DLC = data.size();
	header.ExtId = id;
    std::copy(data.begin(), data.end(), datatx);
	header.IDE = CAN_ID_EXT;
	header.RTR = CAN_RTR_DATA;
    
    
    uint32_t msgBox;
    // send message using a timeout (in ticks)
    
    while (HAL_CAN_GetTxMailboxesFreeLevel(&hcan) == 0);
    HAL_CAN_AddTxMessage(&hcan, &header, datatx, &msgBox);
    // TODO: handle errors
}

/**
 * This is a forwarded interrupt origining from 
 * 1. HAL_CAN_IRQHandler
 * 2. CAN_Receive_IT
 * 3. HAL_CAN_RxCpltCallback (this)
 * @see stm32f1xx_hal_can.c
 */
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef* hcan)
{
    HAL_CAN_DeactivateNotification(hcan, CAN_IT_RX_FIFO0_MSG_PENDING);
    lowLevel::CAN::notify();
}

void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef* hcan)
{
    HAL_CAN_DeactivateNotification(hcan, CAN_IT_RX_FIFO1_MSG_PENDING);
    lowLevel::CAN::notify();
}

bool lowLevel::CAN::receive(lowLevel::CAN::can_id_t& id, 
    lowLevel::CAN::data_t& data, bool& remote_request)
{
    uint8_t datarx[8];
    CAN_RxHeaderTypeDef header;
    if (HAL_CAN_GetRxMessage(&hcan, CAN_RX_FIFO0, &header, datarx) != HAL_OK)
    {
        if (HAL_CAN_GetRxMessage(&hcan, CAN_RX_FIFO1, &header, datarx) != HAL_OK)
        {
            return false;
        }
        else
        {
            HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO1_MSG_PENDING);
        }
    }
    else
    {
        HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO0_MSG_PENDING);
    }
    
    id.id = 0;
    id.stdId = header.StdId;
    id.extId = header.ExtId;
    remote_request = (header.RTR == CAN_RTR_REMOTE);
    
    for (unsigned int i = 0; i < header.DLC; i++)
    {
        data.push_back(datarx[i]);
    }
    return true;
}

void HAL_CAN_ErrorCallback(CAN_HandleTypeDef* hcan)
{
    while(true) {};
}
