//std

//lib

//local
#include <lowLevel/TemperatureSensors.hpp>
#include "tm_stm32_onewire.h"
#include "stm32f1xx.h"
#include "main.h"

#define DS18B20_START_CONVERSION	0x44

using namespace lowLevel;

static TM_OneWire_t oneWireStruct;
static etl::vector<std::array<uint8_t,8>,TemperatureSensors::MAX> addresses;

void TemperatureSensors::init()
{
    TM_OneWire_Init(&oneWireStruct, ONEWIRE_GPIO_Port, ONEWIRE_Pin);
    if (TM_OneWire_First(&oneWireStruct))
    {
        std::array<uint8_t,8> address;
        TM_OneWire_GetFullROM(&oneWireStruct, address.data());
        addresses.push_back(address);
        
        while(TM_OneWire_Next(&oneWireStruct))
        {
            TM_OneWire_GetFullROM(&oneWireStruct, &address[0]);
            addresses.push_back(address);
        }
    }
}

void TemperatureSensors::startConversion()
{
    
    // start conversion on all sensors at once. count conversion time in
    // application logic
    TM_OneWire_Reset(&oneWireStruct);
    TM_OneWire_WriteByte(&oneWireStruct, ONEWIRE_CMD_SKIPROM);
    TM_OneWire_WriteByte(&oneWireStruct, DS18B20_START_CONVERSION);
}

void TemperatureSensors::pollSensors(etl::vector<TemperatureSensors::Message, 
    TemperatureSensors::MAX>& temperatures)
{
    // assume that all conversions has been done
    for (auto& address : addresses)
    {
        Message tm;
        tm.id[0] = address[1];
        tm.id[1] = address[2];
        tm.id[2] = address[3];
        tm.id[3] = address[4];
        tm.id[4] = address[5];
        tm.id[5] = address[6];
        
        struct ds18b20Decoded_t {
            uint16_t temperature;
            uint8_t reserved[6];
            uint8_t crc;
        };
        
        union
        {
            uint8_t bytes[9];
            ds18b20Decoded_t decoded;

        };
        //__disable_irq();
        TM_OneWire_Reset(&oneWireStruct);
        TM_OneWire_Select(&oneWireStruct, address.data());
        TM_OneWire_WriteByte(&oneWireStruct, ONEWIRE_CMD_RSCRATCHPAD);
        
        // receive 9 bytes
        for (auto& byte : bytes)
        {
            byte = TM_OneWire_ReadByte(&oneWireStruct);
        }
        
        auto crc = TM_OneWire_CRC8(bytes, 8);
        //__enable_irq();
        // skip defect data. 
        if (crc != decoded.crc)
        {
            continue;
        }
        
        //divide by 16 to get the temperature
        tm.temperature = decoded.temperature;
        temperatures.push_back(tm);
    }    
}
