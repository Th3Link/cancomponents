//std
#include <limits>
#include <algorithm>
//lib
#include <static_assert/printed_assert.hpp>
//local
#include <lowLevel/InternalFlash.hpp>
#include <stm32f1xx_hal.h>

extern void* __start_signature_region;
extern void* __start_device_info_0_reigon;
extern void* __start_device_info_1_reigon;

const void* lowLevel::InternalFlash::signature = &__start_signature_region;
const void* lowLevel::InternalFlash::device_info_0 = &__start_device_info_0_reigon;
const void* lowLevel::InternalFlash::device_info_1 = &__start_device_info_1_reigon;

const uint8_t* lowLevel::InternalFlash::APPLICATION_START_PTR = 
    reinterpret_cast<uint8_t*>(lowLevel::InternalFlash::FLASH_BASE_ADDR + 
    lowLevel::InternalFlash::APPLICATION_START_ADDR);

constexpr uint32_t address_to_index(uint32_t address)
{
    return address / FLASH_PAGE_SIZE;
}

constexpr uint32_t index_to_page(uint32_t index)
{
    return lowLevel::InternalFlash::FLASH_BASE_ADDR + index * FLASH_PAGE_SIZE;
}

constexpr uint32_t pages(uint32_t address, uint32_t length)
{
    return address_to_index(address+length-1) -
        address_to_index(address) + 1;
}

constexpr FLASH_EraseInitTypeDef erase_structure(uint32_t address, uint32_t length)
{
    return FLASH_EraseInitTypeDef {
        FLASH_TYPEERASE_PAGES,
        FLASH_BANK_1,
        index_to_page(address_to_index(address)),
        pages(address, length) };
        
}

STATIC_ASSERT_EQUAL(address_to_index(0x4000+0), 16);
STATIC_ASSERT_EQUAL(address_to_index(0x4000+888), 16);
STATIC_ASSERT_EQUAL(address_to_index(0x4000+1888), 17);

STATIC_ASSERT_EQUAL(index_to_page(0), lowLevel::InternalFlash::FLASH_BASE_ADDR);
STATIC_ASSERT_EQUAL(index_to_page(32), 0x8008000);
STATIC_ASSERT_EQUAL(index_to_page(33), 0x8008400);

STATIC_ASSERT_EQUAL(pages(0x4000,10), 1);
STATIC_ASSERT_EQUAL(pages(0x4000,1024), 1);
STATIC_ASSERT_EQUAL(pages(0x4000,1025), 2);
STATIC_ASSERT_EQUAL(pages(0x4000+100,1025), 2);
STATIC_ASSERT_EQUAL(pages(0x4000+20222,10465), 45-35+1);

void lowLevel::InternalFlash::erase(const void* a, uint32_t length)
{
    uint32_t address = reinterpret_cast<uint32_t>(a);
    __asm volatile ("cpsid i"); /* Set PRIMASK */
    HAL_FLASH_Unlock();
    FLASH_EraseInitTypeDef eraseStruct = erase_structure(address, length);
    uint32_t PageError = 0;
    HAL_FLASHEx_Erase(&eraseStruct, &PageError);
    HAL_FLASH_Lock();
    __asm volatile ("cpsie i"); /* Clear PRIMASK */
}

void lowLevel::InternalFlash::write(const void* a, const uint8_t* data, uint32_t length)
{
    uint32_t address = reinterpret_cast<uint32_t>(a);
    __asm volatile ("cpsid i"); /* Set PRIMASK */
    HAL_FLASH_Unlock();
    constexpr uint32_t DOUBLE_WORD_LEN = 8;
    int32_t remaining = length;
    uint32_t current_index = 0;
    while (remaining > 0)
    {
        union {
            uint64_t data64;
            uint8_t data8[DOUBLE_WORD_LEN];
        };
        
        data64 = std::numeric_limits<uint64_t>::max();
        
        for (int32_t i = 0; i < std::min(static_cast<int32_t>(DOUBLE_WORD_LEN), remaining); i++)
        {
            data8[i] = data[i+current_index];
        }
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, 
            address + lowLevel::InternalFlash::FLASH_BASE_ADDR + current_index, data64);
        remaining -= DOUBLE_WORD_LEN;
        current_index += DOUBLE_WORD_LEN;
    }
    HAL_FLASH_Lock();
    __asm volatile ("cpsie i"); /* Clear PRIMASK */
}

uint32_t lowLevel::InternalFlash::read(const void* a, uint8_t* data, uint32_t length)
{
    uint32_t address = reinterpret_cast<uint32_t>(a);
    auto flash_data = reinterpret_cast<uint8_t*>(address + lowLevel::InternalFlash::FLASH_BASE_ADDR);
    for (uint32_t i = 0; i < length; i++)
    {
        data[i] = flash_data[i];
    }
    return length;
}
