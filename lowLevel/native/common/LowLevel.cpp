//std
#include <array>
#include <algorithm>

//lib

//local
#include <lowLevel/LowLevel.hpp>
#include "stm32f1xx_hal.h"

using namespace lowLevel;
uint32_t __attribute__((section (".bootflags"))) bootflags_crc[2];

//extern IWDG_HandleTypeDef hiwdg;

void LowLevel::init()
{
    
}

void LowLevel::keepAlive()
{
    // The STM32 Independent Watchdog has its own dedicated clock running at 40kHz
    // (1ms per tick). Prescaler should be configured in cubeMx with 16 and the reload
    // value set to 0xFFF (4095). This results in a timeout of 1.6ms (see reference manual)
    // so we plan to feed every 500ms is plenty enough. Feeding is done over the message
    // queue and queues in iterative. Misses in queueing leads to a restart.
    //HAL_IWDG_Refresh(&hiwdg);
}

void LowLevel::sleep()
{
    
}

void LowLevel::reset()
{
    HAL_NVIC_SystemReset();
}

std::array<uint8_t, 12> LowLevel::getID()
{
    union {
        uint32_t i[3];
        uint8_t uc[12];
    } devid;

    devid.i[0] = HAL_GetUIDw0();
    devid.i[1] = HAL_GetUIDw1();
    devid.i[2] = HAL_GetUIDw2();
    
    std::array<uint8_t, 12> id {0};
    std::copy(std::begin(devid.uc), std::end(devid.uc), std::begin(id));
    
    return id;
}

uint32_t LowLevel::bootflags()
{
    return bootflags_crc[0];
}

uint32_t LowLevel::bootflagCRC()
{
    return bootflags_crc[1];
}

void LowLevel::bootflags(uint32_t bf)
{
    bootflags_crc[0] = bf;
}

void LowLevel::bootflagCRC(uint32_t crc)
{
    bootflags_crc[1] = crc;
}
