/**	
 * |----------------------------------------------------------------------
 * | Copyright (c) 2016 Tilen Majerle
 * |  
 * | Permission is hereby granted, free of charge, to any person
 * | obtaining a copy of this software and associated documentation
 * | files (the "Software"), to deal in the Software without restriction,
 * | including without limitation the rights to use, copy, modify, merge,
 * | publish, distribute, sublicense, and/or sell copies of the Software, 
 * | and to permit persons to whom the Software is furnished to do so, 
 * | subject to the following conditions:
 * | 
 * | The above copyright notice and this permission notice shall be
 * | included in all copies or substantial portions of the Software.
 * | 
 * | THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * | EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * | OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * | AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * | HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * | WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * | FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * | OTHER DEALINGS IN THE SOFTWARE.
 * |----------------------------------------------------------------------
 */
#include "tm_stm32_onewire.h"
#include <chrono>
#include "stm32f1xx_hal_tim.h"
#include "main.h"
extern TIM_HandleTypeDef htim3;

void ONEWIRE_LOW()
{
  ONEWIRE_GPIO_Port->BSRR = (ONEWIRE_Pin << 16);
}

void ONEWIRE_HIGH()
{
  ONEWIRE_GPIO_Port->BSRR = ONEWIRE_Pin;
}

void ONEWIRE_INPUT()
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  GPIO_InitStruct.Pin = ONEWIRE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ONEWIRE_GPIO_Port, &GPIO_InitStruct);
}

static inline void ONEWIRE_OUTPUT()
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  GPIO_InitStruct.Pin = ONEWIRE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(ONEWIRE_GPIO_Port, &GPIO_InitStruct);
}

static inline int ONEWIRE_GETINPUT()
{
    return HAL_GPIO_ReadPin(ONEWIRE_GPIO_Port, ONEWIRE_Pin);
}

void TM_OneWire_Init(TM_OneWire_t* OneWireStruct, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{ 
  ONEWIRE_INPUT();
}

enum action_t {
  NONE,
  PULL,
  RELEASE,
  READ,
  WAIT
};

enum input_t : uint8_t {
  NOT_SET = 0, HIGH = 1, LOW = 2
};

volatile static bool input = false;
volatile static bool wait_sem = false;
extern "C" void TIM3_IRQHandler(void)
{
  /* Output compare 1 event --> RELEASE */
  if (__HAL_TIM_GET_FLAG(&htim3, TIM_FLAG_CC1) != RESET)
  {
      __HAL_TIM_CLEAR_IT(&htim3, TIM_IT_CC1);
      ONEWIRE_INPUT();
      return;
  }
  
  /* Output compare 2 event --> READ */
  if (__HAL_TIM_GET_FLAG(&htim3, TIM_FLAG_CC2) != RESET)
  {
      __HAL_TIM_CLEAR_IT(&htim3, TIM_IT_CC2);
      input = ONEWIRE_GETINPUT();
      return;
  }
  
  /* Output compare 3 event --> WAIT */
  if (__HAL_TIM_GET_FLAG(&htim3, TIM_FLAG_CC3) != RESET)
  {
      __HAL_TIM_CLEAR_IT(&htim3, TIM_IT_CC3);
      /* Reset the CCxE Bit */
      htim3.Instance->CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E);
      __HAL_TIM_DISABLE(&htim3);
      wait_sem = false;
      return;
  }
  
  /* Output compare 4 event --> PULL LOW */
  if (__HAL_TIM_GET_FLAG(&htim3, TIM_FLAG_CC4) != RESET)
  {
      __HAL_TIM_CLEAR_IT(&htim3, TIM_IT_CC4);
      ONEWIRE_LOW();
      ONEWIRE_OUTPUT();
      return;
  }
}

input_t timer_based_bit_transfer(uint16_t relaese, uint16_t read, uint16_t wait)
{
  wait_sem = true;
  // configure timer
  
  __HAL_TIM_DISABLE(&htim3);
  TIM_CHANNEL_STATE_SET(&htim3, TIM_CHANNEL_ALL, HAL_TIM_CHANNEL_STATE_BUSY);
  
  htim3.Instance->SR = 0;
  
  TIM_OC_InitTypeDef sConfigOC = {0};
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;
  sConfigOC.Pulse = relaese;
  HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1);
  sConfigOC.Pulse = read;
  HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2);
  sConfigOC.Pulse = wait;
  HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_3);
  sConfigOC.Pulse = 1;
  HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_4);
  
  __HAL_TIM_SET_COUNTER(&htim3, 0);
  

  // pull down line, needed for every operation

  // start timer

  __HAL_TIM_DISABLE_IT(&htim3, TIM_IT_UPDATE);
  __HAL_TIM_DISABLE_IT(&htim3, TIM_IT_BREAK);
  __HAL_TIM_DISABLE_IT(&htim3, TIM_IT_COM);

  /* Enable the TIM Capture/Compare 1 interrupt */
  __HAL_TIM_ENABLE_IT(&htim3, TIM_IT_CC1);
  TIM_CCxChannelCmd(htim3.Instance, TIM_CHANNEL_1, TIM_CCx_ENABLE);
  if (read != 0)
  {
    __HAL_TIM_ENABLE_IT(&htim3, TIM_IT_CC2);
    TIM_CCxChannelCmd(htim3.Instance, TIM_CHANNEL_2, TIM_CCx_ENABLE);
  }
  else
  {
    __HAL_TIM_DISABLE_IT(&htim3, TIM_IT_CC2);
  }
  __HAL_TIM_ENABLE_IT(&htim3, TIM_IT_CC3);
  TIM_CCxChannelCmd(htim3.Instance, TIM_CHANNEL_3, TIM_CCx_ENABLE);
  __HAL_TIM_ENABLE_IT(&htim3, TIM_IT_CC4);
  TIM_CCxChannelCmd(htim3.Instance, TIM_CHANNEL_4, TIM_CCx_ENABLE);

  __HAL_TIM_ENABLE(&htim3);
  
  while (wait_sem);
  if (read == 0)
  {
    return input_t::NOT_SET;
  }
  return (input ? input_t::HIGH : input_t::LOW);
}

uint8_t TM_OneWire_Reset(TM_OneWire_t* OneWireStruct)
{	
  if (timer_based_bit_transfer(480, 70+480, 410+70+480) == input_t::HIGH)
  {
    return 1;
  }
  return 0;
}

void TM_OneWire_WriteBit(TM_OneWire_t* OneWireStruct, uint8_t bit)
{

  if (bit) {
    timer_based_bit_transfer(6, 0, 64+6);
	} else {
		// values from the data sheet
    timer_based_bit_transfer(60, 0, 60+10);
	}
}

uint8_t TM_OneWire_ReadBit(TM_OneWire_t* OneWireStruct)
{
  if (timer_based_bit_transfer(6, 9+6, 55+6+9) == input_t::HIGH)
  {
    return 1;
  }
  return 0;
}

void TM_OneWire_WriteByte(TM_OneWire_t* OneWireStruct, uint8_t byte) {
	uint8_t i = 8;
	/* Write 8 bits */
	while (i--) {
		/* LSB bit is first */
		TM_OneWire_WriteBit(OneWireStruct, byte & 0x01);
		byte >>= 1;
	}
}

uint8_t TM_OneWire_ReadByte(TM_OneWire_t* OneWireStruct) {
	uint8_t i = 8, byte = 0;
	while (i--) {
		byte >>= 1;
		byte |= (TM_OneWire_ReadBit(OneWireStruct) << 7);
	}
	
	return byte;
}

uint8_t TM_OneWire_First(TM_OneWire_t* OneWireStruct) {
	/* Reset search values */
	TM_OneWire_ResetSearch(OneWireStruct);

	/* Start with searching */
	return TM_OneWire_Search(OneWireStruct, ONEWIRE_CMD_SEARCHROM);
}

uint8_t TM_OneWire_Next(TM_OneWire_t* OneWireStruct) {
   /* Leave the search state alone */
   return TM_OneWire_Search(OneWireStruct, ONEWIRE_CMD_SEARCHROM);
}

void TM_OneWire_ResetSearch(TM_OneWire_t* OneWireStruct) {
	/* Reset the search state */
	OneWireStruct->LastDiscrepancy = 0;
	OneWireStruct->LastDeviceFlag = 0;
	OneWireStruct->LastFamilyDiscrepancy = 0;
}

uint8_t TM_OneWire_Search(TM_OneWire_t* OneWireStruct, uint8_t command) {
	uint8_t id_bit_number;
	uint8_t last_zero, rom_byte_number, search_result;
	uint8_t id_bit, cmp_id_bit;
	uint8_t rom_byte_mask, search_direction;

	/* Initialize for search */
	id_bit_number = 1;
	last_zero = 0;
	rom_byte_number = 0;
	rom_byte_mask = 1;
	search_result = 0;

	/* Check if any devices */
	if (!OneWireStruct->LastDeviceFlag) {
		/* 1-Wire reset */
		if (TM_OneWire_Reset(OneWireStruct)) {
			/* Reset the search */
			OneWireStruct->LastDiscrepancy = 0;
			OneWireStruct->LastDeviceFlag = 0;
			OneWireStruct->LastFamilyDiscrepancy = 0;
			return 0;
		}

		/* Issue the search command */
		TM_OneWire_WriteByte(OneWireStruct, command);  

		/* Loop to do the search */
		do {
			/* Read a bit and its complement */
			id_bit = TM_OneWire_ReadBit(OneWireStruct);
			cmp_id_bit = TM_OneWire_ReadBit(OneWireStruct);

			/* Check for no devices on 1-wire */
			if ((id_bit == 1) && (cmp_id_bit == 1)) {
				break;
			} else {
				/* All devices coupled have 0 or 1 */
				if (id_bit != cmp_id_bit) {
					/* Bit write value for search */
					search_direction = id_bit;
				} else {
					/* If this discrepancy is before the Last Discrepancy on a previous next then pick the same as last time */
					if (id_bit_number < OneWireStruct->LastDiscrepancy) {
						search_direction = ((OneWireStruct->ROM_NO[rom_byte_number] & rom_byte_mask) > 0);
					} else {
						/* If equal to last pick 1, if not then pick 0 */
						search_direction = (id_bit_number == OneWireStruct->LastDiscrepancy);
					}
					
					/* If 0 was picked then record its position in LastZero */
					if (search_direction == 0) {
						last_zero = id_bit_number;

						/* Check for Last discrepancy in family */
						if (last_zero < 9) {
							OneWireStruct->LastFamilyDiscrepancy = last_zero;
						}
					}
				}

				/* Set or clear the bit in the ROM byte rom_byte_number with mask rom_byte_mask */
				if (search_direction == 1) {
					OneWireStruct->ROM_NO[rom_byte_number] |= rom_byte_mask;
				} else {
					OneWireStruct->ROM_NO[rom_byte_number] &= ~rom_byte_mask;
				}
				
				/* Serial number search direction write bit */
				TM_OneWire_WriteBit(OneWireStruct, search_direction);

				/* Increment the byte counter id_bit_number and shift the mask rom_byte_mask */
				id_bit_number++;
				rom_byte_mask <<= 1;

				/* If the mask is 0 then go to new SerialNum byte rom_byte_number and reset mask */
				if (rom_byte_mask == 0) {
					rom_byte_number++;
					rom_byte_mask = 1;
				}
			}
		/* Loop until through all ROM bytes 0-7 */
		} while (rom_byte_number < 8);

		/* If the search was successful then */
		if (!(id_bit_number < 65)) {
			/* Search successful so set LastDiscrepancy, LastDeviceFlag, search_result */
			OneWireStruct->LastDiscrepancy = last_zero;

			/* Check for last device */
			if (OneWireStruct->LastDiscrepancy == 0) {
				OneWireStruct->LastDeviceFlag = 1;
			}

			search_result = 1;
		}
	}

	/* If no device found then reset counters so next 'search' will be like a first */
	if (!search_result || !OneWireStruct->ROM_NO[0]) {
		OneWireStruct->LastDiscrepancy = 0;
		OneWireStruct->LastDeviceFlag = 0;
		OneWireStruct->LastFamilyDiscrepancy = 0;
		search_result = 0;
	}

	return search_result;
}

int TM_OneWire_Verify(TM_OneWire_t* OneWireStruct) {
	unsigned char rom_backup[8];
	int i,rslt,ld_backup,ldf_backup,lfd_backup;

	/* Keep a backup copy of the current state */
	for (i = 0; i < 8; i++)
	rom_backup[i] = OneWireStruct->ROM_NO[i];
	ld_backup = OneWireStruct->LastDiscrepancy;
	ldf_backup = OneWireStruct->LastDeviceFlag;
	lfd_backup = OneWireStruct->LastFamilyDiscrepancy;

	/* Set search to find the same device */
	OneWireStruct->LastDiscrepancy = 64;
	OneWireStruct->LastDeviceFlag = 0;

	if (TM_OneWire_Search(OneWireStruct, ONEWIRE_CMD_SEARCHROM)) {
		/* Check if same device found */
		rslt = 1;
		for (i = 0; i < 8; i++) {
			if (rom_backup[i] != OneWireStruct->ROM_NO[i]) {
				rslt = 1;
				break;
			}
		}
	} else {
		rslt = 0;
	}

	/* Restore the search state */
	for (i = 0; i < 8; i++) {
		OneWireStruct->ROM_NO[i] = rom_backup[i];
	}
	OneWireStruct->LastDiscrepancy = ld_backup;
	OneWireStruct->LastDeviceFlag = ldf_backup;
	OneWireStruct->LastFamilyDiscrepancy = lfd_backup;

	/* Return the result of the verify */
	return rslt;
}

void TM_OneWire_TargetSetup(TM_OneWire_t* OneWireStruct, uint8_t family_code) {
   uint8_t i;

	/* Set the search state to find SearchFamily type devices */
	OneWireStruct->ROM_NO[0] = family_code;
	for (i = 1; i < 8; i++) {
		OneWireStruct->ROM_NO[i] = 0;
	}
	
	OneWireStruct->LastDiscrepancy = 64;
	OneWireStruct->LastFamilyDiscrepancy = 0;
	OneWireStruct->LastDeviceFlag = 0;
}

void TM_OneWire_FamilySkipSetup(TM_OneWire_t* OneWireStruct) {
	/* Set the Last discrepancy to last family discrepancy */
	OneWireStruct->LastDiscrepancy = OneWireStruct->LastFamilyDiscrepancy;
	OneWireStruct->LastFamilyDiscrepancy = 0;

	/* Check for end of list */
	if (OneWireStruct->LastDiscrepancy == 0) {
		OneWireStruct->LastDeviceFlag = 1;
	}
}

uint8_t TM_OneWire_GetROM(TM_OneWire_t* OneWireStruct, uint8_t index) {
	return OneWireStruct->ROM_NO[index];
}

void TM_OneWire_Select(TM_OneWire_t* OneWireStruct, uint8_t* addr) {
	uint8_t i;
	TM_OneWire_WriteByte(OneWireStruct, ONEWIRE_CMD_MATCHROM);
	
	for (i = 0; i < 8; i++) {
		TM_OneWire_WriteByte(OneWireStruct, *(addr + i));
	}
}

void TM_OneWire_SelectWithPointer(TM_OneWire_t* OneWireStruct, uint8_t *ROM) {
	uint8_t i;
	TM_OneWire_WriteByte(OneWireStruct, ONEWIRE_CMD_MATCHROM);
	
	for (i = 0; i < 8; i++) {
		TM_OneWire_WriteByte(OneWireStruct, *(ROM + i));
	}	
}

void TM_OneWire_GetFullROM(TM_OneWire_t* OneWireStruct, uint8_t *firstIndex) {
	uint8_t i;
	for (i = 0; i < 8; i++) {
		*(firstIndex + i) = OneWireStruct->ROM_NO[i];
	}
}

uint8_t TM_OneWire_CRC8(uint8_t *addr, uint8_t len) {
	uint8_t crc = 0, inbyte, i, mix;
	
	while (len--) {
		inbyte = *addr++;
		for (i = 8; i; i--) {
			mix = (crc ^ inbyte) & 0x01;
			crc >>= 1;
			if (mix) {
				crc ^= 0x8C;
			}
			inbyte >>= 1;
		}
	}
	
	/* Return calculated CRC */
	return crc;
}
