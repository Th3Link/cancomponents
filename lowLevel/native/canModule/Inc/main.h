/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void GPIO_BUTTON_Init(void);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define IRIN_Pin GPIO_PIN_1
#define IRIN_GPIO_Port GPIOD
#define DIMMWERT1_Pin GPIO_PIN_0
#define DIMMWERT1_GPIO_Port GPIOA
#define DIMMWERT2_Pin GPIO_PIN_1
#define DIMMWERT2_GPIO_Port GPIOA
#define DIMMWERT3_Pin GPIO_PIN_2
#define DIMMWERT3_GPIO_Port GPIOA
#define DIMMWERT4_Pin GPIO_PIN_3
#define DIMMWERT4_GPIO_Port GPIOA
#define TEMP_ERROR1_Pin GPIO_PIN_4
#define TEMP_ERROR1_GPIO_Port GPIOA
#define TEMP_ERROR2_Pin GPIO_PIN_5
#define TEMP_ERROR2_GPIO_Port GPIOA
#define TEMP_ERROR3_Pin GPIO_PIN_6
#define TEMP_ERROR3_GPIO_Port GPIOA
#define TEMP_ERROR4_Pin GPIO_PIN_7
#define TEMP_ERROR4_GPIO_Port GPIOA
#define LED1_Pin GPIO_PIN_0
#define LED1_GPIO_Port GPIOB
#define LED2_Pin GPIO_PIN_1
#define LED2_GPIO_Port GPIOB
#define CON5_P6_Pin GPIO_PIN_2
#define CON5_P6_GPIO_Port GPIOB
#define CON5_P5_Pin GPIO_PIN_3
#define CON5_P5_GPIO_Port GPIOB
#define CON5_P5_EXTI_IRQn EXTI3_IRQn
#define CON5_P4_Pin GPIO_PIN_4
#define CON5_P4_GPIO_Port GPIOB
#define CON5_P4_EXTI_IRQn EXTI4_IRQn
/* USER CODE BEGIN Private defines */
#define CON5_P3_Pin GPIO_PIN_5
#define CON5_P3_GPIO_Port GPIOB
#define CON5_P3_EXTI_IRQn EXTI9_5_IRQn
#define CON5_P2_Pin GPIO_PIN_6
#define CON5_P2_GPIO_Port GPIOB
#define CON5_P2_EXTI_IRQn EXTI9_5_IRQn
#define CON5_P1_Pin GPIO_PIN_7
#define CON5_P1_GPIO_Port GPIOB
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
