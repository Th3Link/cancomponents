//std

//lib

//local
#include <lowLevel/Sensors.hpp>
#include <stm32f1xx_hal.h>
#include <main.h>

constexpr uint16_t someI2CAdress = 0x5A;
constexpr uint32_t trials = 3;
constexpr uint32_t timeout = 100;

extern I2C_HandleTypeDef hi2c1;

void lowLevel::Sensors::init()
{
    
}

bool lowLevel::Sensors::available()
{
    return (HAL_I2C_IsDeviceReady(&hi2c1, someI2CAdress, trials, timeout) == HAL_OK);
    //return false;
}
