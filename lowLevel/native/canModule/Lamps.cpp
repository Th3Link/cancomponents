//std
#include <array>
#include <limits>

//lib

//local
#include <lowLevel/Lamps.hpp>
#include <stm32f1xx_hal.h>

extern TIM_HandleTypeDef htim2;

struct lamps_pwm_t
{
    lowLevel::Lamps::number_t number;
    TIM_HandleTypeDef* handle;
    uint32_t channel;
};

std::array<lowLevel::Lamps::state_t, lowLevel::Lamps::count> lampstates;

const std::array<lamps_pwm_t, lowLevel::Lamps::count> lamps_pwm_lookup {{
    {0, &htim2, TIM_CHANNEL_1},
    {1, &htim2, TIM_CHANNEL_2},
    {2, &htim2, TIM_CHANNEL_3},
    {3, &htim2, TIM_CHANNEL_4},
}};
    
void lowLevel::Lamps::init(uint16_t pwm_frequency)
{
    HAL_TIM_Base_Start(&htim2);
    
    for (auto& lamps_pwm : lamps_pwm_lookup)
    {
        value(lamps_pwm.number, 0);
    }
}

void lowLevel::Lamps::value(lowLevel::Lamps::number_t number, lowLevel::Lamps::state_t state)
{
    if (number > lowLevel::Lamps::count)
    {
        //TODO: log error
        return;
    }
    
    lampstates[number] = state;
    
    uint32_t pulse = htim2.Init.Period - (state * htim2.Init.Period) / 
        std::numeric_limits<uint16_t>::max();
    
    TIM_OC_InitTypeDef sConfigOC = {0};
    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = static_cast<uint16_t>(pulse);
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCNPolarity = TIM_OCNPOLARITY_LOW;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    sConfigOC.OCIdleState = TIM_OCIDLESTATE_SET;
    sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_SET;
    HAL_TIM_PWM_Stop(lamps_pwm_lookup[number].handle, 
        lamps_pwm_lookup[number].channel);
    HAL_TIM_PWM_ConfigChannel(lamps_pwm_lookup[number].handle, 
        &sConfigOC, lamps_pwm_lookup[number].channel);
    HAL_TIM_PWM_Start(lamps_pwm_lookup[number].handle, 
        lamps_pwm_lookup[number].channel);
}

lowLevel::Lamps::state_t lowLevel::Lamps::value(lowLevel::Lamps::number_t number)
{
    if (number > lowLevel::Lamps::count)
    {
        //TODO: log error
        return 0;
    }
    return lampstates[number];
}
