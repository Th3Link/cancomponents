//std
#include <array>

//lib

//local
#include <lowLevel/Button.hpp>
#include <stm32f1xx_hal.h>
#include <main.h>

struct button_config_t
{
    GPIO_TypeDef* gpio;
    uint16_t pin;
    lowLevel::Button::button_t button;
};

std::array<button_config_t, lowLevel::Button::BUTTON_COUNT> button_config
{{
    {CON5_P2_GPIO_Port, CON5_P2_Pin, lowLevel::Button::button_t::UPPER_LEFT},
    {CON5_P3_GPIO_Port, CON5_P3_Pin, lowLevel::Button::button_t::LOWER_LEFT},
    {CON5_P4_GPIO_Port, CON5_P4_Pin, lowLevel::Button::button_t::UPPER_RIGHT},
    {CON5_P5_GPIO_Port, CON5_P5_Pin, lowLevel::Button::button_t::LOWER_RIGHT}
}};

void lowLevel::Button::init()
{
    //this automatically deactivates I2C communication
    GPIO_BUTTON_Init();
}

lowLevel::Button::state_t lowLevel::Button::read(lowLevel::Button::button_t button)
{
    for (auto b : button_config)
    {
        if (b.button == button)
        {
            if (HAL_GPIO_ReadPin(b.gpio, b.pin) == GPIO_PIN_RESET)
            {
                return lowLevel::Button::state_t::PRESS;
            }
            else
            {
                return lowLevel::Button::state_t::RELEASED;
            }
        }
    }
    
    // the program never goes here because there will always be a button up there so
    // just for the compiler warning return the released state.
    return lowLevel::Button::state_t::RELEASED;
}

/* callback from EXTI triggers on falling edge (when the button grounds the pulled up pin)
 * That callback is only used to signal the application to start polling te buttons
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    for (auto b : button_config)
    {
        if (HAL_GPIO_ReadPin(b.gpio, b.pin) == GPIO_PIN_RESET)
        {
            lowLevel::Button::event(b.button, lowLevel::Button::state_t::PRESS);
        }
    }
}
