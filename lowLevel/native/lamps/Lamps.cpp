//std
#include <array>
#include <etl/vector.h>
#include <algorithm>    // std::sort
#include <limits>

//lib

//local
#include <lowLevel/Lamps.hpp>
#include "stm32f1xx_hal.h"
#include "main.h"

extern TIM_HandleTypeDef htim4;
extern DMA_HandleTypeDef hdma_tim4_ch1;
extern DMA_HandleTypeDef hdma_tim4_ch2;
extern DMA_HandleTypeDef hdma_tim4_ch3;
extern DMA_HandleTypeDef hdma_tim4_up;

using namespace lowLevel;

struct lamps_pwm_t
{
    uint8_t index;
    uint16_t pin;
};

constexpr size_t GPIO_A_NUMBER = 0;
constexpr size_t GPIO_B_NUMBER = 1;
constexpr size_t GPIO_C_NUMBER = 2;
constexpr size_t GPIO_D_NUMBER = 3;


constexpr uint32_t PORTX_PWM_SIZE = 128;

static uint32_t portX_pwm[4][PORTX_PWM_SIZE] {0};

constexpr uint8_t port_index(GPIO_TypeDef* port)
{
    if (port == GPIOB)
    {
        return GPIO_B_NUMBER;
    }
    else if (port == GPIOC)
    {
        return GPIO_C_NUMBER;
    }
    else if (port == GPIOD)
    {
        return GPIO_D_NUMBER;
    }
    else
    {
        return GPIO_A_NUMBER;
    }
}

constexpr GPIO_TypeDef* index_port(uint8_t index)
{
    if (index == GPIO_B_NUMBER)
    {
        return GPIOB;
    }
    else if (index == GPIO_C_NUMBER)
    {
        return GPIOC;
    }
    else if (index == GPIO_D_NUMBER)
    {
        return GPIOD;
    }
    else
    {
        return GPIOA;
    }
}

const std::array<lamps_pwm_t, Lamps::count> lamps_pwm_lookup {{
    {port_index(PWM07_GPIO_Port), PWM07_Pin},
    {port_index(PWM06_GPIO_Port), PWM06_Pin},
    {port_index(PWM05_GPIO_Port), PWM05_Pin},
    {port_index(PWM04_GPIO_Port), PWM04_Pin},
    {port_index(PWM03_GPIO_Port), PWM03_Pin},
    {port_index(PWM02_GPIO_Port), PWM02_Pin},
    {port_index(PWM01_GPIO_Port), PWM01_Pin},
    {port_index(PWM00_GPIO_Port), PWM00_Pin},
    {port_index(PWM13_GPIO_Port), PWM13_Pin},
    {port_index(PWM12_GPIO_Port), PWM12_Pin},
    {port_index(PWM14_GPIO_Port), PWM14_Pin},
    {port_index(PWM15_GPIO_Port), PWM15_Pin},
    {port_index(PWM08_GPIO_Port), PWM08_Pin},
    {port_index(PWM09_GPIO_Port), PWM09_Pin},
    {port_index(PWM10_GPIO_Port), PWM10_Pin},
    {port_index(PWM11_GPIO_Port), PWM11_Pin},
    {port_index(PWM20_GPIO_Port), PWM20_Pin},
    {port_index(PWM21_GPIO_Port), PWM21_Pin},
    {port_index(PWM22_GPIO_Port), PWM22_Pin},
    {port_index(PWM23_GPIO_Port), PWM23_Pin},
    {port_index(PWM16_GPIO_Port), PWM16_Pin},
    {port_index(PWM17_GPIO_Port), PWM17_Pin},
    {port_index(PWM18_GPIO_Port), PWM18_Pin},
    {port_index(PWM19_GPIO_Port), PWM19_Pin}
}};

void Lamps::init(uint16_t pwm_frequency)
{
    for (auto& l : lamps_pwm_lookup)
    {
        portX_pwm[l.index][0] |= (l.pin << 16);
    }
    
    HAL_DMA_Start(&hdma_tim4_ch1, reinterpret_cast<uint32_t>(&portX_pwm[GPIO_D_NUMBER][0]), 
      reinterpret_cast<uint32_t>(&GPIOD->BSRR), PORTX_PWM_SIZE);
    HAL_DMA_Start(&hdma_tim4_ch2, reinterpret_cast<uint32_t>(&portX_pwm[GPIO_C_NUMBER][0]), 
      reinterpret_cast<uint32_t>(&GPIOC->BSRR), PORTX_PWM_SIZE);
    HAL_DMA_Start(&hdma_tim4_ch3, reinterpret_cast<uint32_t>(&portX_pwm[GPIO_B_NUMBER][0]), 
      reinterpret_cast<uint32_t>(&GPIOB->BSRR), PORTX_PWM_SIZE);
    HAL_DMA_Start(&hdma_tim4_up, reinterpret_cast<uint32_t>(&portX_pwm[GPIO_A_NUMBER][0]), 
      reinterpret_cast<uint32_t>(&GPIOA->BSRR), PORTX_PWM_SIZE);
      
    __HAL_TIM_ENABLE_DMA(&htim4, TIM_DMA_CC1);
    __HAL_TIM_ENABLE_DMA(&htim4, TIM_DMA_CC2);
    __HAL_TIM_ENABLE_DMA(&htim4, TIM_DMA_CC3);
    __HAL_TIM_ENABLE_DMA(&htim4, TIM_DMA_UPDATE);

    TIM_CCxChannelCmd(htim4.Instance, TIM_CHANNEL_1, TIM_CCx_ENABLE);
    TIM_CCxChannelCmd(htim4.Instance, TIM_CHANNEL_2, TIM_CCx_ENABLE);
    TIM_CCxChannelCmd(htim4.Instance, TIM_CHANNEL_3, TIM_CCx_ENABLE);
    TIM_CCxChannelCmd(htim4.Instance, TIM_CHANNEL_4, TIM_CCx_ENABLE);
    
    htim4.Instance->CR2 |= TIM_CR2_CCDS;
    
  __HAL_TIM_ENABLE(&htim4);
      
}

extern "C" void TIM4_IRQHandler(void)
{
    __HAL_TIM_CLEAR_IT(&htim4, TIM_IT_UPDATE);
    __HAL_TIM_CLEAR_IT(&htim4, TIM_IT_CC1);
    __HAL_TIM_CLEAR_IT(&htim4, TIM_IT_CC2);
    __HAL_TIM_CLEAR_IT(&htim4, TIM_IT_CC3);
    __HAL_TIM_CLEAR_IT(&htim4, TIM_IT_CC4);
    __HAL_TIM_CLEAR_IT(&htim4, TIM_IT_BREAK);
}

void Lamps::value(Lamps::number_t number, Lamps::state_t state)
{
    // temporary max limit
    if (state > 128)
    {
        state = 128;
    }
    // min limits
    if (state > 0 && state < 16)
    {
        state = 17;
    }
    state /= 2; //align to 128 steps
    
    for (uint32_t i = 0; i < PORTX_PWM_SIZE; i++)
    {
        portX_pwm[lamps_pwm_lookup[number].index][i] &= ~(lamps_pwm_lookup[number].pin);
    }
    if (state > 0)
    {
      portX_pwm[lamps_pwm_lookup[number].index][PORTX_PWM_SIZE-state] |= (lamps_pwm_lookup[number].pin);
    }
}


Lamps::state_t Lamps::value(Lamps::number_t number)
{
    for (uint32_t i = 0; i < PORTX_PWM_SIZE; i++)
    {
        if (portX_pwm[lamps_pwm_lookup[number].index][i] & lamps_pwm_lookup[number].pin)
        {
          return i;
        }
    }
    return 0;
}
