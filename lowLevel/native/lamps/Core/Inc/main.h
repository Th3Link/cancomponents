/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define PWM22_Pin GPIO_PIN_13
#define PWM22_GPIO_Port GPIOC
#define PWM23_Pin GPIO_PIN_14
#define PWM23_GPIO_Port GPIOC
#define PWM08_Pin GPIO_PIN_15
#define PWM08_GPIO_Port GPIOC
#define PWM09_Pin GPIO_PIN_0
#define PWM09_GPIO_Port GPIOD
#define PWM10_Pin GPIO_PIN_1
#define PWM10_GPIO_Port GPIOD
#define PWM11_Pin GPIO_PIN_0
#define PWM11_GPIO_Port GPIOA
#define PWM12_Pin GPIO_PIN_1
#define PWM12_GPIO_Port GPIOA
#define PWM13_Pin GPIO_PIN_2
#define PWM13_GPIO_Port GPIOA
#define PWM14_Pin GPIO_PIN_3
#define PWM14_GPIO_Port GPIOA
#define PWM15_Pin GPIO_PIN_4
#define PWM15_GPIO_Port GPIOA
#define PWM00_Pin GPIO_PIN_5
#define PWM00_GPIO_Port GPIOA
#define PWM01_Pin GPIO_PIN_6
#define PWM01_GPIO_Port GPIOA
#define PWM02_Pin GPIO_PIN_7
#define PWM02_GPIO_Port GPIOA
#define PWM03_Pin GPIO_PIN_0
#define PWM03_GPIO_Port GPIOB
#define PWM04_Pin GPIO_PIN_1
#define PWM04_GPIO_Port GPIOB
#define B2_GND_Pin GPIO_PIN_2
#define B2_GND_GPIO_Port GPIOB
#define PWM06_Pin GPIO_PIN_10
#define PWM06_GPIO_Port GPIOB
#define PWM07_Pin GPIO_PIN_11
#define PWM07_GPIO_Port GPIOB
#define LED0_Pin GPIO_PIN_12
#define LED0_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_13
#define LED1_GPIO_Port GPIOB
#define ONEWIRE_Pin GPIO_PIN_14
#define ONEWIRE_GPIO_Port GPIOB
#define PWM18_Pin GPIO_PIN_15
#define PWM18_GPIO_Port GPIOB
#define PWM19_Pin GPIO_PIN_8
#define PWM19_GPIO_Port GPIOA
#define PWM05_Pin GPIO_PIN_9
#define PWM05_GPIO_Port GPIOA
#define PWM17_Pin GPIO_PIN_10
#define PWM17_GPIO_Port GPIOA
#define PWM16_Pin GPIO_PIN_15
#define PWM16_GPIO_Port GPIOA
#define B3_SW_Pin GPIO_PIN_3
#define B3_SW_GPIO_Port GPIOB
#define B4_SW_Pin GPIO_PIN_4
#define B4_SW_GPIO_Port GPIOB
#define B5_SW_Pin GPIO_PIN_5
#define B5_SW_GPIO_Port GPIOB
#define B6_SCL_SW_Pin GPIO_PIN_6
#define B6_SCL_SW_GPIO_Port GPIOB
#define B7_VCC_SDA_Pin GPIO_PIN_7
#define B7_VCC_SDA_GPIO_Port GPIOB
#define PWM20_Pin GPIO_PIN_8
#define PWM20_GPIO_Port GPIOB
#define PWM21_Pin GPIO_PIN_9
#define PWM21_GPIO_Port GPIOB
void   MX_CAN_Init(void);
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
