//std
#include <array>

//lib

//local
#include <lowLevel/Relais.hpp>
#include "stm32f1xx.h"

struct relais_gpio_t
{
    GPIO_TypeDef* port;
    uint16_t pin;
};

std::array<relais_gpio_t, 12> relaisGPIOs {{
    {GPIOA, GPIO_PIN_2},
    {GPIOA, GPIO_PIN_1},
    {GPIOA, GPIO_PIN_0},
    {GPIOD, GPIO_PIN_1},
    {GPIOD, GPIO_PIN_0},
    {GPIOC, GPIO_PIN_15},
    {GPIOC, GPIO_PIN_14},
    {GPIOC, GPIO_PIN_13},
    {GPIOB, GPIO_PIN_9},
    {GPIOB, GPIO_PIN_8},
    {GPIOB, GPIO_PIN_7},
    {GPIOB, GPIO_PIN_6},
}};

using namespace lowLevel;

void Relais::init()
{
    for (auto& relaisGPIO : relaisGPIOs)
    {
        GPIO_InitTypeDef GPIO_InitStruct;
        GPIO_InitStruct.Pin = relaisGPIO.pin;
        GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
        HAL_GPIO_Init(relaisGPIO.port, &GPIO_InitStruct);
        HAL_GPIO_WritePin(relaisGPIO.port, relaisGPIO.pin,GPIO_PIN_RESET);
    }
}

void Relais::set(uint8_t id, bool state)
{
    if (id >= relaisGPIOs.size())
    {
        /* TODO some error handling could be useful. Just make it right,
         * this saves work
         */
        return;
    }
    HAL_GPIO_WritePin(relaisGPIOs[id].port, relaisGPIOs[id].pin, 
        (state ? GPIO_PIN_SET : GPIO_PIN_RESET));
}

bool Relais::get(uint8_t id)
{
    if (id >= relaisGPIOs.size())
    {
        /* TODO some error handling could be useful. Just make it right,
         * this saves work
         */
        return false;
    }
    return (HAL_GPIO_ReadPin(relaisGPIOs[id].port, 
        relaisGPIOs[id].pin) == GPIO_PIN_SET);
}
