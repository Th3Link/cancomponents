#ifndef __CAN_HPP__
#define __CAN_HPP__

//std
#include <etl/vector.h>
#include <cstdint>
//lib

//local

namespace lowLevel
{
    namespace CAN
    {
        
        // structs are defined LSB->MSB (top to bottom)
        struct can_id_t {
            union {
                uint32_t id;
                struct {
                    uint32_t reserved0 : 3;
                    uint32_t extId : 18;
                    uint32_t stdId : 11;
                };
                struct {
                    uint32_t reserved1 : 3;
                    uint32_t msg_id : 8;
                    uint32_t device_id : 8;
                    uint32_t type : 6;
                    uint32_t group : 6;
                    uint32_t ng_bit : 1;
                };
            };
        };
        
        namespace BAUDRATE_SETTING
        {
            constexpr uint8_t BAUDRATE_DEFAULT_50_KBITS = 0;
            constexpr uint8_t BAUDRATE_22_222KBITS = 1;
            constexpr uint8_t BAUDRATE_25KBITS = 2;
            constexpr uint8_t BAUDRATE_50KBITS = 3;
            constexpr uint8_t BAUDRATE_100KBITS = 4;
        }
        
        constexpr unsigned int BYTES_PER_MESSAGE = 8;
        // for shorter writing
        using data_t = etl::vector<uint8_t, BYTES_PER_MESSAGE>;
        void init(uint8_t baudrate);
        void setFilter(const uint8_t device_id, const uint8_t device_type, const uint8_t group[6]);
        void send(uint32_t id, data_t& data);
        bool receive(can_id_t& id, data_t& data, bool& remote_request);
        void notify();
    }
}

#endif //__CAN_HPP__
