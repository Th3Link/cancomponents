#ifndef __BOOTLOADER_INTERNAL_FLASH_HPP__
#define __BOOTLOADER_INTERNAL_FLASH_HPP__

//std
#include <cstdint>
//lib

//local

namespace lowLevel
{
    namespace InternalFlash
    {
        constexpr uint32_t FLASH_BASE_ADDR = 0x8000000;
        constexpr uint32_t SIGNATURE_START_ADDR = 0x3C00;
        constexpr uint32_t BOOTLOADER_START_ADDR = 0x0;
        constexpr uint32_t BOOTLOADER_SIZE = 13*1024;
        constexpr uint32_t APPLICATION_START_ADDR = 0x4000;
        constexpr uint32_t APPLICATION_MAX_SIZE = 48*1024;
        extern const uint8_t* APPLICATION_START_PTR;
            
        extern const void* signature;
        extern const void* device_info_0;
        extern const void* device_info_1;
            
        void erase(const void* /*address*/, const uint32_t length);
        void write(const void* /*address*/, const uint8_t* data, const uint32_t length);
        uint32_t read(const void* /*address*/, uint8_t* data, uint32_t length); 
    }
}
#endif //__BOOTLOADER_INTERNAL_FLASH_HPP__
