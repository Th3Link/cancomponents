#ifndef __LOWLEVEL_HPP__
#define __LOWLEVEL_HPP__

//std
#include <cstdint>
#include <array>
//lib

//local

namespace lowLevel
{
    namespace LowLevel
    {       
        void init();
        void keepAlive();
        void sleep();
        void reset();
        std::array<uint8_t, 12> getID();
        uint32_t bootflags();
        uint32_t bootflagCRC();
        void bootflags(uint32_t);
        void bootflagCRC(uint32_t);
    }
}

#endif //__LOWLEVEL_HPP__
