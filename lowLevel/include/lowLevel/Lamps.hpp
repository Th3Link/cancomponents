#ifndef __LOWLEVEL_LAMPS_HPP__
#define __LOWLEVEL_LAMPS_HPP__

//std
#include <cstdint>

//lib

//local

namespace lowLevel
{
namespace Lamps
{
    constexpr uint8_t count = 24;
    using number_t = uint8_t;
    using state_t = uint8_t;
    void init(uint16_t pwm_frequency);
    void value(number_t, state_t);
    state_t value(number_t);
}
}
#endif //__LOWLEVEL_LAMPS_HPP__
