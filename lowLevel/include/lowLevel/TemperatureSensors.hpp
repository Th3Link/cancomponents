#ifndef __TEMPERATURE_SENSORS_HPP__
#define __TEMPERATURE_SENSORS_HPP__

//std
#include <cstdint>
#include <etl/vector.h>
#include <array>

//lib

//local


namespace lowLevel
{
    namespace TemperatureSensors
    {
        constexpr unsigned int MAX = 30;
        struct Message
        {
            uint8_t id[6];
            uint16_t temperature;
        };
        void init();
        void startConversion();
        void pollSensors(etl::vector<Message, MAX>& temperatures);
    }
}

#endif //__TEMPERATURE_SENSORS_HPP__
