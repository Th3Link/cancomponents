#ifndef __RELAIS_HPP__
#define __RELAIS_HPP__

//std
#include <cstdint>

//lib

//local

namespace lowLevel
{
    namespace Relais
    {
        void init();
        void set(uint8_t id, bool state);
        bool get(uint8_t id);
    }
}

#endif //__RELAIS_HPP__
