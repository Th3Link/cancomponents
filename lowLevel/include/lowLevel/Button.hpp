#ifndef __LOWLEVEL_BUTTON_HPP__
#define __LOWLEVEL_BUTTON_HPP__

//std

//lib

//local

namespace lowLevel
{
    namespace Button
    {
        enum button_t
        {
            UPPER_LEFT,
            UPPER_RIGHT,
            LOWER_LEFT,
            LOWER_RIGHT
        };
        
        constexpr int BUTTON_COUNT = 4;
        
        enum state_t
        {
            RELEASED,
            PRESS
        };
        
        void init();
        state_t read(button_t);
        void event(button_t, state_t);
    }
}

#endif //__LOWLEVEL_BUTTON_HPP__
