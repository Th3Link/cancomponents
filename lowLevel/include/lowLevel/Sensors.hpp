#ifndef __LOWLEVEL_SENSORS_HPP__
#define __LOWLEVEL_SENSORS_HPP__

//std

//lib

//local

namespace lowLevel
{
    namespace Sensors
    {
        void init();
        bool available();
    }
}

#endif //__LOWLEVEL_SENSORS_HPP__
