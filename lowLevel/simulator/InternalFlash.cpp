//std

//lib

//local
#include <lowLevel/InternalFlash.hpp>


const void* lowLevel::InternalFlash::signature = 0;
const void* lowLevel::InternalFlash::device_info_0 = 0;
const void* lowLevel::InternalFlash::device_info_1 = 0;

const uint8_t* lowLevel::InternalFlash::APPLICATION_START_PTR = reinterpret_cast<uint8_t*>(0x8004000);


void lowLevel::InternalFlash::erase(const void* address, const uint32_t length)
{

}

void lowLevel::InternalFlash::write(const void* address, const uint8_t* data, const uint32_t length)
{

}

uint32_t lowLevel::InternalFlash::read(const void* address, uint8_t* data, uint32_t length)
{
    return 0;
}
