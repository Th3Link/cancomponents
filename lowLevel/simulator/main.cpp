#include <lowLevel/applicationHook.h>
#include <pybind11/pybind11.h>
#include <iostream>

extern "C" int main(void);

int main()
{
    std::cout << "startApplication" << std::endl;
    startApplication();
    
    while(1) {
        
    }
}

PYBIND11_MODULE(canmodule, s) {
    s.doc() = "simulator test"; // optional module docstring

    s.def("main", &main, "main call blah");
}
