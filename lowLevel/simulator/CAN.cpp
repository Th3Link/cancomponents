//std
#include <algorithm>

//lib

//local
#include <lowLevel/CAN.hpp>
#include <pybind11/pybind11.h>

using namespace lowLevel;

// structs are defined LSB->MSB (top to bottom)
struct can_id_t {
    union {
        uint32_t id;
        struct {
            uint32_t reserved : 3;
            uint32_t extId : 18;
            uint32_t stdId : 11;
        };
    };
};

void lowLevel::CAN::init(uint8_t baudrate)
{
    
}

void lowLevel::CAN::setFilter(const uint8_t device_id, const uint8_t device_type, const uint8_t group[6])
{
    
}

void lowLevel::CAN::send(uint32_t id, CAN::data_t& data)
{
    
}

PYBIND11_MODULE(can, s) {
    s.doc() = "simulator test"; // optional module docstring

    s.def("send", &lowLevel::CAN::send, "send data over CAN");
    s.def("receive", &lowLevel::CAN::receive, "send data over CAN");
}
