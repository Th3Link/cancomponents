//std

//lib

//local
#include <lowLevel/LowLevel.hpp>

using namespace lowLevel;

void LowLevel::init()
{

}

void LowLevel::keepAlive()
{
    
}

void LowLevel::sleep()
{
    
}

void LowLevel::reset()
{
    
}

std::array<uint8_t, 12> LowLevel::getID()
{
    std::array<uint8_t, 12> id {0x5a};
    return id;
}
