import can
import signal
import argparse
import os
import time

max_filesize = 49*1024

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()

def auto_int(x):
    return int(x,0)

def send(can_id, data=[], remote=True):
    bus.send(can.Message(arbitration_id=can_id, data=data,is_extended_id=True,is_remote_frame=remote))


def signal_handler(signum, frame):
    raise Exception("Timeout")

def hexstr(a):
    return ''.join(format(x, '02x') for x in a)

def wait_for(can_id, timeout=2):
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(timeout)
    msg = None
    while True:
        while msg is None:
            msg = reader.get_message(timeout=0.01)
        if msg.arbitration_id == can_id:
            signal.alarm(0)
            return msg.data
        else:
            msg = None

def read_application_version(data):
    major = data[0] << 8 | data[1]
    minor = data[2] << 8 | data[3]
    return (major, minor)

def main():
    parser = argparse.ArgumentParser(description='Update CAN Firmware')
    parser.add_argument('can_id', metavar='CAN_ID', type=auto_int, help='CAN to for the device to be updated')
    parser.add_argument('-i', '--interface', metavar='INTERFACE', type=str, default='can0', help='name of the can interface (socketcan)')
    parser.add_argument('-u', '--upload_file', metavar='UPDATE_FILE', type=str, help='update file (*.bin)')
    parser.add_argument('-d', '--download_file', metavar='DOWNLOAD_FILE', type=str, help='readback file (*.bin)')
    args = parser.parse_args()

    bus = can.interface.Bus(bustype='socketcan', channel=args.interface,bitrate=50000)
    bus.set_filters([{'can_id': args.can_id,'can_mask':0x1FFFFF00,'extended':True}])
    reader = can.BufferedReader()
    notifier = can.Notifier(bus, [reader])
    
    
    recv = wait_for(args.can_id | 0, 3600);
    
    send(args.can_id | 0);
    while recv[0] != 0x02:
        send(args.can_id | 0x2, data=[0x2], remote=False)
        time.sleep(0.1)
        recv = wait_for(args.can_id | 0);
    send(args.can_id | 3);
    print('CPU_ID0: {}'.format(hexstr(wait_for(args.can_id | 3))))
    send(args.can_id | 4);
    print('CPU_ID1: {}'.format(hexstr(wait_for(args.can_id | 4))))
    send(args.can_id | 7);
    (major, minor) = read_application_version(wait_for(args.can_id | 7))
    print('APPLICATION_VERSION: {}.{}'.format(major, minor))
    
    if args.upload_file is not None:
        upload(args.can_id, args. upload_file)
    
    if args.download_file is not None:
        download(args.can_id, args.download_file)
    
def upload(can_id, upload_file):
    # upload new firmware into device flash
    filesize = os.path.getsize(upload_file)
    send(can_id | 16, [
        0,
        0,
        0,
        0,
        (filesize >> 24) & 0xFF,
        (filesize >> 16) & 0xFF,
        (filesize >> 8) & 0xFF,
        filesize & 0xFF],remote=False);

    if filesize > max_filesize:
        raise Exception('input file in too large: is {}, max {}'.format(filesize, max_filesize))
    send(can_id | 20)
    print('checksum of original FLASH: {}'.format(hexstr(wait_for(can_id | 20))))
    send(can_id | 17, data=[], remote=False)
    send(can_id | 20)
    print('checksum of empty FLASH: {}'.format(hexstr(wait_for(can_id | 20))))
    
    with open(upload_file, 'rb') as f:
        i = 0
        printProgressBar(i, filesize/8, prefix = 'Upload:', suffix = 'Complete', length = 50)
        chunk = f.read(8)
        while chunk:
            send(can_id | 19, data=chunk, remote=False)
            i = i + 1;
            printProgressBar(i, filesize/8, prefix = 'Upload:', suffix = 'Complete', length = 50)
            chunk = f.read(8)
            time.sleep(0.02)
    # newline helps not to write into the progress bar
    print('')
    send(can_id | 20)
    print('checksum of uploaded FLASH: {}'.format(hexstr(wait_for(can_id | 20, 10))))
        

def download(can_id, download_file):
    # download firmware from device flash
    if download_file is not None:
        send(can_id | 16, [
            0,
            0,
            0,
            0,
            (max_filesize >> 24) & 0xFF,
            (max_filesize >> 16) & 0xFF,
            (max_filesize >> 8) & 0xFF,
            max_filesize & 0xFF],remote=False);
        read_bytes = []
        i = 0
        printProgressBar(i, max_filesize/8, prefix = 'Download:', suffix = 'Complete', length = 50)
        while len(read_bytes) < max_filesize:
            send(can_id | 18)
            b = wait_for(can_id | 18, 10)
            i = i + 1
            printProgressBar(i, max_filesize/8, prefix = 'Download:', suffix = 'Complete', length = 50)
            read_bytes.extend(b)
    
        print('')
        with open(download_file, 'wb') as f:
            f.write(bytearray(read_bytes))

    send(can_id | 0x2, data=[0x0], remote=False)


except Exception as e:
    raise(e)

