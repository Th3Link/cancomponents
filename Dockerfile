FROM archlinux:latest

RUN patched_glibc=glibc-linux4-2.33-4-x86_64.pkg.tar.zst && \
    curl -LO "https://repo.archlinuxcn.org/x86_64/$patched_glibc" && \
    bsdtar -C / -xvf "$patched_glibc"

RUN pacman -Syu --noconfirm \
&& pacman -S --noconfirm arm-none-eabi-newlib arm-none-eabi-gcc arm-none-eabi-binutils gcc \
scons \
cppcheck \
plantuml \
python-kivy \
python python-pip \
&& pacman -Scc --noconfirm \
&& pip install sly intelhex jinja2 pybind11
COPY srecord-1.64-2-x86_64.pkg.tar.xz srecord-1.64-2-x86_64.pkg.tar.xz
RUN pacman -U --noconfirm srecord-1.64-2-x86_64.pkg.tar.xz && rm srecord-1.64-2-x86_64.pkg.tar.xz
