#!/bin/bash
printf "0: %.8x" $1 | sed -E 's/0: (..)(..)(..)(..)/0: \4\3\2\1/'| xxd -r -g0 > /tmp/id.bin
st-flash --debug --reset write /tmp/id.bin 0x8003000
st-flash --debug --reset read /tmp/id2.bin 0x8003000 4
xxd /tmp/id.bin > /tmp/id.hex
xxd /tmp/id2.bin > /tmp/id2.hex
diff /tmp/id.hex /tmp/id2.hex
rm /tmp/id.bin /tmp/id2.bin /tmp/id.hex /tmp/id2.hex
