import can
import signal
import argparse
import os
import time

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()


def auto_int(x):
    return int(x,0)

def send(can_id, data=[], remote=True):
    return bus.send(can.Message(arbitration_id=can_id, data=data,is_extended_id=True,is_remote_frame=remote))


def signal_handler(signum, frame):
    raise Exception("Timeout")

def wait_for(can_id, timeout=2):
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(timeout)
    msg = None
    while True:
        while msg is None:
            msg = reader.get_message()
        if msg.arbitration_id == can_id:
            signal.alarm(0)
            return msg.data

def hexstr(a):
    return ''.join(format(x, '02x') for x in a)

parser = argparse.ArgumentParser(description='Update CAN Firmware')
parser.add_argument('can_id', metavar='CAN_ID', type=auto_int, help='CAN to for the device to be updated')
parser.add_argument('update_file', metavar='UPDATE_FILE', type=str, help='update file (*.bin)')
parser.add_argument('download_file', metavar='DOWNLOAD_FILE', type=str, help='readback file (*.bin)')
args = parser.parse_args()

bus = can.interface.Bus(bustype='socketcan', channel='can0',bitrate=50000)
reader = can.BufferedReader()
notifier = can.Notifier(bus, [reader])

filesize = os.path.getsize(args.update_file)
max_filesize = 49*1024
if filesize > max_filesize:
    raise Exception('input file in too large: is {}, max {}'.format(filesize, max_filesize))

try:
    send(args.can_id | 0);
    recv = wait_for(args.can_id | 0);
    while recv[0] != 0x02:
        send(args.can_id | 0x2, data=[0x2], remote=False)
        time.sleep(0.1)
        recv = wait_for(args.can_id | 0);
    send(args.can_id | 3);
    print('CPU_ID0: {}'.format(hexstr(wait_for(args.can_id | 3))))
    send(args.can_id | 4);
    print('CPU_ID1: {}'.format(hexstr(wait_for(args.can_id | 4))))
    send(args.can_id | 16, [
        0,
        0,
        0,
        0,
        (filesize >> 24) & 0xFF,
        (filesize >> 16) & 0xFF,
        (filesize >> 8) & 0xFF,
        filesize & 0xFF],remote=False);
    send(args.can_id | 20)
    print('checksum of original FLASH: {}'.format(hexstr(wait_for(args.can_id | 20))))
    send(args.can_id | 17, data=[], remote=False)
    send(args.can_id | 20)
    print('checksum of empty FLASH: {}'.format(hexstr(wait_for(args.can_id | 20))))
    
    with open(args.update_file, 'rb') as f:
        i = 0
        printProgressBar(i, filesize/8, prefix = 'Upload:', suffix = 'Complete', length = 50)
        chunk = f.read(8)
        while chunk:
            send(args.can_id | 19, data=chunk, remote=False)
            i = i + 1;
            printProgressBar(i, filesize/8, prefix = 'Upload:', suffix = 'Complete', length = 50)
            chunk = f.read(8)
            time.sleep(0.05)
     
    send(args.can_id | 20)
    print('checksum of uploaded FLASH: {}'.format(hexstr(wait_for(args.can_id | 20))))
    
    read_bytes = []
    with open(args.update_file, 'rb') as f:
        i = 0
        printProgressBar(i, filesize/8, prefix = 'Download:', suffix = 'Complete', length = 50)
        while len(readbytes) < filesize:
            send(args.can_id | 18)
            b = wait_for(args.can_id | 18)
            
            i = i + 1
            printProgressBar(i, filesize/8, prefix = 'Download:', suffix = 'Complete', length = 50)
            
            if b != f.read(len(b)):
                print('readfile not equal after {} bytes'.format(len(read_bytes)))
            read_bytes.extend(b)
    
    with open(args.download_file, 'wb') as f:
        t.write(read_bytes)



except Exception as e:
    raise(e)
