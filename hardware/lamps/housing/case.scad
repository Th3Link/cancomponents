$fn=90;
segment1_d = [82.2+5,41.5+2];
complete_d = [segment1_d[0],93];
hole_r = 5/2-0.3;
holes_d = 77-2*(5/2);
holes_to_segment1 = 2.5+hole_r;
segment2_d= [68,complete_d[1]-segment1_d[1]];
corner_space_d = [11,7.5];
height = 8;
t=6;

clip_spacer_holes = 33.8;

mount_holes_r1 = 4.5/2;
mount_holes_r2 = 10/2;

tht_space = 2;

module bottom(h, border=0, s1_diff=0) {
    translate([border+s1_diff/2, border,0]) cube([segment1_d[0]-s1_diff-2*border,segment1_d[1]-2*border,h]);
    translate([border+1, 0,0]) translate([(segment1_d[0]-segment2_d[0])/2, segment1_d[1],0]) cube([segment2_d[0]-2*border,segment2_d[1]-border,h]);
}
module clips(depth = 0.6)
{
    translate([-depth,0,0])  hull() {
    translate([0,-3,1.5]) sphere(1.5, $fa=5, $fs=0.1); 
    translate([0,segment1_d[1],1.5]) sphere(1.5, $fa=5, $fs=0.1);
    }
    translate([complete_d[0]+depth,0,0]) hull() {
    translate([0,-3,1.5]) sphere(1.5, $fa=5, $fs=0.1); 
    translate([0,segment1_d[1],1.5]) sphere(1.5, $fa=5, $fs=0.1);
    }
    translate([0,complete_d[1],0]) hull() {
    translate([-depth,depth,1.5]) sphere(1.5, $fa=5, $fs=0.1); 
    translate([segment1_d[0]+depth,depth,1.5]) sphere(1.5, $fa=5, $fs=0.1); 
    }
}

module pins() {
    grow = 0.4;
    shrink = 1.2;
    h = 4;
    slot_d = 1.5;
    slot_h = 5;
    module pins2() {
    difference() {
    intersection() {
    union() {
        cylinder(r1=hole_r,r2=hole_r+grow,h=h, $fn=30);
        translate([0,0,h]) cylinder(r1=hole_r+grow,r2=hole_r,h=h, $fn=30);
    }
    translate([-hole_r-grow,-hole_r+shrink/2,0]) cube([3*hole_r,2*hole_r-shrink,h*2]);
    }
    translate([-slot_d/2,-hole_r-grow,(2*h-slot_h)/2]) cube([slot_d,2*(hole_r+grow),slot_h]);
    }
    }
    pins2();
    translate([holes_d,0,0]) pins2();
}

module mount_holes()
{
    translate([0,0,t-mount_holes_r2-tht_space])cylinder(r1=0, r2=mount_holes_r2, h=mount_holes_r2, $fn=30);
    cylinder(r=mount_holes_r1, h=t, $fn=30);
}

module triangleCube(h1,h2,l,t)
{
    h = h1 > h2 ? h1 : h2;    
    rot_dir = (h1-h2)/abs(h1-h2);
    dh = abs(h1-h2);
    c = sqrt(dh*dh+l*l);
    angle = asin(dh/c);
    difference() {
        cube([t,l,h]);
        translate([0,0,h2]) rotate([rot_dir*angle,0,0]) cube([t,c,c/2]);
    }
}

module base() {
    translate([5.15+3,7.13,3]) pins();
    // angeld hold blocks
    intersection()
    {
        union() {
    translate([10,84.5,t]) rotate([0,0,45]) cube([11,11,1.6]);
    translate([11,83.5,t+1.6]) rotate([0,0,45]) cube([11,11,2]);
    translate([85.2,92.2,t]) rotate([0,0,135]) cube([11,11,1.6]);
     translate([84.2,91.2,t+1.6]) rotate([0,0,135]) cube([11,11,2]);
        }
        bottom(h=30);
    }
    difference() {
        union() {
            bottom(h=t);
            translate([0,0,0]) cube([complete_d[0],2,30]);
            translate([0,0,0]) triangleCube(t,30,segment1_d[1],2);
            translate([segment1_d[0]-2,0,0]) triangleCube(t,30,segment1_d[1],2);
        }
        translate([0,0,t-tht_space]) bottom(h=t, border = 1.5, s1_diff=3);
        translate([complete_d[0]/2,18,0]) mount_holes();
        translate([complete_d[0]/2,78,0]) mount_holes();
        clips();
        
        //center breakout
        translate([25,-5,0]) cube([35,15,30]);
        
        for (i = [0,1,2,8,9,10]) {
        translate([(i+1)*5+(i*2.4)+1,2,24]) rotate([90,0,0]) cylinder(r=6.5/2, h=10);
        }
        for (i = [0 : 2]) {
        translate([5*(i+1)+(i*2)+1,2,15]) rotate([90,0,0]) cylinder(r=6.5/2, h=10);
        }
        for (i = [0 : 2]) {
        translate([5*(i+1)+(i*2.5)+60,2,15]) rotate([90,0,0]) cylinder(r=6.5/2, h=10);
        }
        translate([(segment1_d[0]-clip_spacer_holes)/2,45,0]) union() {
        cylinder(r=3/2,h=t);
        translate([clip_spacer_holes,0,0]) cylinder(r=3/2,h=t);
        }
    }
}

module lid() {
    difference() {
    union() {
    intersection() {
        clips();
        bottom(h=10, border=-1.5);
    }
     difference() {
        translate([0,0,31.2]) bottom(h=1, border=-1.5);
    }
    difference() {
        bottom(h=32.2, border=-1.5);
        bottom(h=32.2, border=-0.2);
        translate([-5,complete_d[1]-2,0]) cube([complete_d[0]+10,2,15]);
        translate([segment1_d[0]-2,0,0]) cube([2,complete_d[1],15]);
        translate([2,0,0]) cube([2,complete_d[1],15]);
    }
}
    translate([2,-5,0]) cube([segment1_d[0]-4,30,29]);
}
}
//translate([131.25,-36,5]) rotate([0,0,180]) import("pcb.stl", convexity = 5);
base();
//lid();