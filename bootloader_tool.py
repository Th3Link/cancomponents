#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import can
import signal
import argparse
import os
import time
import sys

max_filesize = 49*1024

def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
            It must be "yes" (the default), "no" or None (meaning
            an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == "":
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' " "(or 'y' or 'n').\n")

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, 
        fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()

def signal_handler(signum, frame):
    raise Exception("Timeout")

def auto_int(x):
    return int(x,0)

def hexstr(a):
    return ''.join(format(x, '02x') for x in a)

def print_groups(groups):
    return '0x' + ' 0x'.join(format(x, '02x') for x in groups)

def read_application_version(data):
    major = data[0] << 8 | data[1]
    minor = data[2] << 8 | data[3]
    return (major, minor)

def decode_device_id_type(data):
    return (data[0], data[1])

class ParameterCheckException(Exception):
    """Exception raised for errors in the input salary.

    Attributes:
        salary -- input salary which caused the error
        message -- explanation of the error
    """

    def __init__(self, message="Paratemer incorrect"):
        self.message = message
        super().__init__(self.message)

class bootloader_tool:
    def __init__(self, can_id, interface, bitrate, non_interactive=False, rescue_run=False,
            upload_file=None, download_file=None, device_id=None, device_type=None, 
            groups=None, description_file=None):
        self.can_id = can_id
        self.non_interactive = non_interactive
        self.rescue_run = rescue_run
        self.upload_file = upload_file
        self.download_file = download_file
        self.device_id = device_id
        self.device_type = device_type
        self.groups = groups
        self.description_file = description_file
        self.bus = can.interface.Bus(bustype='socketcan', channel=interface,
            bitrate=bitrate)
        #self.bus.set_filters([{'can_id': self.can_id,'can_mask':0x1FFFFF00,'extended':True}])
        self.reader = can.BufferedReader()
        self.notifier = can.Notifier(self.bus, [self.reader])
        
        if (self.device_type is not None and self.device_id is None) or (self.device_id is not None and 
                self.device_type is None):
            raise ParameterCheckException("--device_type und --device_id must be set together")
            if self.device_id < 0 or self.device_id > 0xFF:
                raise ParameterCheckException("--device_id must be in range (0 .. 0xFF/255)")

            if self.device_type < 0 or self.device_type > 0x3F:
                raise ParameterCheckException("--device_type must be in range (0 .. 0x3F/63)")
        if self.groups is not None:
            if len(self.groups) > 6:
                raise ParameterCheckException("only 6 groups can be set")
            for i in self.groups:
                if i < 0 or i > 0x3F:
                    raise ParameterCheckException("--groups must be in range (0 .. 0x3F/63)")
        
        if self.can_id & 0x10000000 == 0:
            raise ParameterCheckException("highest bit must be set (ng-bit)")
        
        if self.can_id == 0x10000000 and (self.upload_file is not None or self.download_file is not None or 
                self.groups is not None):
            raise ParameterCheckException('upload, download or group set is not allowed on broadcast (0x10000xx)')

        if self.can_id & 0x3FFF00 == 0 and (self.upload_file is not None or self.download_file is not None):
            raise ParameterCheckException('upload, download is not allowed on group broadcast (0x1xx000xx)')
    
    def send(self, message_id, data=[], remote=True, can_id=None):
        if can_id is None:
            can_id = self.can_id
        self.bus.send(can.Message(arbitration_id=(can_id | message_id), data=data,is_extended_id=True,is_remote_frame=remote))

    def wait_for(self, message_id, timeout=2, can_id=None):
        if can_id is None:
            can_id = self.can_id
        signal.signal(signal.SIGALRM, signal_handler)
        signal.alarm(timeout)
        msg = None
        while True:
            while msg is None:
                msg = self.reader.get_message(timeout=0.01)
            if msg.arbitration_id == (can_id | message_id):
                signal.alarm(0)
                return msg.data
            else:
                msg = None
                
    def start(self):
        if self.rescue_run:
            self.rescue()
        else:
            self.reboot(0x02)
            
        self.send(3)
        raw_cpu_id0 = self.wait_for(3)
        cpu_id0 = hexstr(raw_cpu_id0)
        print('CPU_ID0: {}'.format(cpu_id0))
        self.send(4)
        cpu_id1 = hexstr(self.wait_for(4))
        print('CPU_ID1: {}'.format(cpu_id1))
        
        self.send(5)
        (device_id, device_type) = decode_device_id_type(self.wait_for(5))
        print('DEVICE_ID: {}, DEVICE_TYPE: {}'.format(device_id, device_type))
        
        self.send(6)
        groups = self.wait_for(6)
        print('GROUPS: {}'.format(print_groups(groups)))

        self.send(7)
        (major, minor) = read_application_version(self.wait_for(7))
        print('APPLICATION_VERSION: {}.{}'.format(major, minor))
        
        if self.upload_file is not None:
            self.upload()
        
        if self.download_file is not None:
            self.download()
        
        if self.device_id is not None and self.device_type is not None:
            self.device_id_type(raw_cpu_id0)
        
        if self.groups is not None:
            self.program_groups(groups)
        if self.description_file is not None:
            self.write_documentation(cpu_id0, cpu_id1)
        
        self.reboot(0x01)
    
    def rescue(self):
        rescued = False
        while True:
            self.send(0x2, data=[0x2], remote=False)
            time.sleep(0.05)
            
    def reboot(self, mode, new_can_id=None):
        self.send(0x2, data=[mode], remote=False)
        
        if new_can_id is not None:
            self.can_id = new_can_id
        time.sleep(1)
        self.send(0)
        recv = self.wait_for(0,5)
        while recv[0] != mode:
            self.send(0x2, data=[mode], remote=False)
            time.sleep(0.1)
            recv = self.wait_for(0)
            
    def upload(self):
        # upload new firmware into device flash
        filesize = os.path.getsize(self.upload_file)
        self.send(16, [
            0,
            0,
            0,
            0,
            (filesize >> 24) & 0xFF,
            (filesize >> 16) & 0xFF,
            (filesize >> 8) & 0xFF,
            filesize & 0xFF],remote=False);

        if filesize > max_filesize:
            raise Exception('input file in too large: is {}, max {}'.format(filesize, max_filesize))
        self.send(20)
        print('checksum of original FLASH: {}'.format(hexstr(self.wait_for(20))))
        self.send(17, data=[], remote=False)
        self.send(20)
        print('checksum of empty FLASH: {}'.format(hexstr(self.wait_for(20))))
        
        with open(self.upload_file, 'rb') as f:
            i = 0
            printProgressBar(i, filesize/8, prefix = 'Upload:', suffix = 'Complete', length = 50)
            chunk = f.read(8)
            while chunk:
                self.send(19, data=chunk, remote=False)
                i = i + 1;
                printProgressBar(i, filesize/8, prefix = 'Upload:', suffix = 'Complete', length = 50)
                chunk = f.read(8)
                time.sleep(0.02)
        # newline helps not to write into the progress bar
        print('')
        self.send(20)
        print('checksum of uploaded FLASH: {}'.format(hexstr(self.wait_for(20, 10))))
        
        self.send(7);
        (major, minor) = read_application_version(self.wait_for(7))
        print('APPLICATION_VERSION: {}.{}'.format(major, minor))

    def download(self):
        # download firmware from device flash
        if self.download_file is not None:
            self.send(16, [
                0,
                0,
                0,
                0,
                (max_filesize >> 24) & 0xFF,
                (max_filesize >> 16) & 0xFF,
                (max_filesize >> 8) & 0xFF,
                max_filesize & 0xFF],remote=False);
            read_bytes = []
            i = 0
            printProgressBar(i, max_filesize/8, prefix = 'Download:', suffix = 'Complete', length = 50)
            while len(read_bytes) < max_filesize:
                self.send(18)
                b = self.wait_for(18, 10)
                i = i + 1
                printProgressBar(i, max_filesize/8, prefix = 'Download:', suffix = 'Complete', length = 50)
                read_bytes.extend(b)
        
            print('')
            with open(self.download_file, 'wb') as f:
                f.write(bytearray(read_bytes))

    def device_id_type(self, cpu_id0):
        if self.can_id == 0x103FFF00:
            self.send(3, data=cpu_id0, remote=False);
            if self.non_interactive or query_yes_no('Device {} is in Broadcast-Mode (no device_id/device_type is set. Continue?)'.format(
                    hexstr(cpu_id0)), default="no") is False:
                return
        self.send(5, data=[self.device_id, self.device_type], remote=False);
        #reboot into bootloader again (changes in device_id and device_type is applied after update)
        new_can_id = (self.can_id & 0x1FC000FF) | self.device_id << 8 | self.device_type << 16
        self.reboot(0x02, new_can_id)
        self.send(5);
        (new_device_id, new_device_type) = decode_device_id_type(self.wait_for(5))
        
        if (new_device_id != self.device_id or new_device_type != self.device_type):
            raise Exception('failed to write new (device_id, device_type). Should be ({},{}) but is ({},{}).'.format(
                    self.device_id, self.device_type, new_device_id, new_device_type))

    def program_groups(self, old_groups):
        # group      0x01 0x02 0x03
        # old_group  0x04
        # result     0x04 0x02 0x03

        # group      0x01 0x00 0x03 0x05
        # old_group  0x00 0x04
        # result     0x01 0x04 0x03 0x05
        result = []
        for i in range(0,6):
            if i > len(old_groups):
                if i > len(self.groups):
                    result.append(0)
                else:
                    result.append(self.groups[i])
            else:
                if i > len(self.groups):
                    result.append(old_groups[i])
                else:
                    if old_groups == 0:
                        result.append(self.groups[i])
                    elif self.groups == 0:
                        result.append(old_groups[i])
                    elif yes or query_yes_no('Overwrite group {} with {}. Continue?)'.format(
                            old_groups[i], self.groups[i]), default="no") is True:
                        result.append(self.groups[i])
                    else:
                        result.append(old_groups[i])
                        
        self.send(data=result, remote=False)
        #reboot into bootloader again (changes in groups is applied after update)
        self.reboot(0x02)
        
        self.send(6)
        new_groups = self.wait_for(6)
        for i in range(0,len(new_groups)):
            if (new_groups[i] != result[i]):
                raise Exception('failed to write groups. Should be {} but is {}.'.format(
                    result, new_groups))
    def write_documentation(cpu_id0, cpu_id1):
        pass

def main():
    parser = argparse.ArgumentParser(description='Update CAN Firmware')
    parser.add_argument('can_id', metavar='CAN_ID', type=auto_int,
            help='CAN to for the device to be updated')
    parser.add_argument('-y', '--yes', action='store_true', default=False, 
            help='do not ask if unsure, just to it (for automation)')
    parser.add_argument('-i', '--interface', metavar='INTERFACE', type=str, default='can0',
            help='name of the can interface (socketcan)')
    parser.add_argument('-b', '--bitrate', metavar='BITRATE', type=int, default=50000,
            help='bit rate for the can interface)')
    parser.add_argument('-u', '--upload_file', metavar='UPDATE_FILE', type=str,
            help='update file (*.bin)')
    parser.add_argument('-d', '--download_file', metavar='DOWNLOAD_FILE', type=str,
            help='readback file (*.bin)')
    parser.add_argument('-r', '--rescue_run', action='store_true', default=False, 
            help='do a rescue run because the firmware does not run anymore. Start this script first an then power cycle the device')
    parser.add_argument('--device_id', metavar='DEVICE_ID', type=auto_int, 
        help='set the device id. must set together with --device_type')
    parser.add_argument('--device_type', metavar='DEVICE_TYPE', type=auto_int,
        help='set the device type. must set together with --device_id')
    parser.add_argument('--groups', metavar='GROUPS', nargs='+', type=auto_int,
        help='programm groups into the device')
    parser.add_argument('--description_file', metavar='DESCRIPTION_FILE', type=auto_int,
        help='append the device id/type changes to this file')
    args = parser.parse_args()
    
    try:
        bt = bootloader_tool(args.can_id, args.interface, args.bitrate, non_interactive=args.yes, 
            rescue_run=args.rescue_run, upload_file=args.upload_file, download_file=args.download_file, 
            device_id=args.device_id, device_type=args.device_type, 
            groups=args.groups, description_file=args.description_file)
        bt.start()
    except ParameterCheckException as e:
        parser.parser_error(e.message)
    except Exception as e:
        raise(e)
    


if __name__ == "__main__":
    # execute only if run as a script
    main()
