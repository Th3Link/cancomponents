from intelhex import IntelHex
import struct
import argparse

parser = argparse.ArgumentParser(description='Generate 0x8004000 CAN bus ID block.')
parser.add_argument('canid', type=int, help='give me the can bus id')
parser.add_argument('out', type=str, help='output file')
args = parser.parse_args()

ih = IntelHex()

baz = struct.pack('I', args.canid)
ih.puts(0x8004000,baz)
print(ih.gets(0x8006000))
print(ih.gets(0x8006040))
print(ih.gets(0x8006080))
print(ih.gets(0x80060A0))
print(ih.gets(0x80060C0))

ih.tofile(args.out, format='hex')