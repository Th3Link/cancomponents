#ifndef __CRC32_HPP__
#define __CRC32_HPP__

#include <cstdint>

namespace CRC32
{
    /*
     * We calculate the table for every operation. It is still fast enough but saves a lot
     * of flash or RAM in case of a static table. However, this requires 2KByte of stack
     * memory
     */
    inline uint32_t compute(const uint8_t* data, uint32_t size, uint32_t* previous_crc = nullptr)
    {
        constexpr uint32_t CRC32_POLYNOMIAL = 0xEDB88320;
        constexpr uint32_t CRC32_TABLE_SIZE = 256;
        static uint32_t tab[CRC32_TABLE_SIZE];
        static bool table_exist = false;
        if (!table_exist)
        {
            for (uint32_t i = 0; i < CRC32_TABLE_SIZE; i++) 
            {
                uint32_t crc = i;
                for (uint32_t j = 0; j < 8; j++)
                {
                    crc = (crc >> 1) ^ (-static_cast<int32_t>(crc & 1) & CRC32_POLYNOMIAL);
                }
                tab[i] = crc; 
            }
            table_exist = true;
        }
        
        uint32_t crc;
        
        crc = (previous_crc == nullptr) ? 0xFFFFFFFF : ~(*previous_crc);
        while (size--) {
            crc = tab[(crc ^ *data++) & 0xFF] ^ (crc >> 8);
        }
        return crc ^ ~0U;
    }
};

#endif //__CRC32_HPP__

