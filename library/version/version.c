#include "version.h"

const version_t m_version __attribute__((section(".version"))) =
{
    .main_version = 0,
    .major_version = 0,
    .bugfix_version = 0
};

extern void* __begin_version;
const version_t* version = (version_t*)&__begin_version;
