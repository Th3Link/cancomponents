#ifndef __VERSION_H__
#define __VERSION_H__

#include <stdint.h>

//generate C legacy code
#ifdef __cplusplus
extern "C"
{
#endif

typedef struct {
    uint16_t main_version;
    uint16_t major_version;
    uint16_t bugfix_version;
    uint16_t reserved;
} version_t;

extern const version_t* version;

//generate C legacy code
#ifdef __cplusplus
}
#endif

#endif //__VERSION_H__
