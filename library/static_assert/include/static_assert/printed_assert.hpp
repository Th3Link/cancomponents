template <long V1, long V2>
struct StaticAssertEqual
{
  static constexpr void* NotEqualError() { return V1 + V2; }
};

template <long V>
struct StaticAssertEqual<V, V>
{
  static constexpr bool NotEqualError = true;
};

#define STATIC_ASSERT_EQUAL(V1, V2)                          \
  static_assert(                                                     \
    StaticAssertEqual<static_cast<long>(V1),                      \
                         static_cast<long>(V2)>::NotEqualError,      \
    #V1 " != " #V2)
