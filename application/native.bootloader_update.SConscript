from intelhex import IntelHex
from struct import *

Import('env')

env_app = env.Clone()
env_app_nosignature = env.Clone()

def getBootloaderLen(env):
    ih = IntelHex()
    ih.loadhex(str(env['bootloader_hex'][0]))
    app = ih[0x8000000:0x8000000+13*1024]
    app.padding = 0xFF
    app_binarr = app.tobinarray()
    return len(app_binarr)

def shift_bootloader(source, target, env):
    ih = IntelHex()
    ih.loadhex(str(source[0]))
    app = ih[0x8000000:0x8000000+13*1024]
    app.padding = 0xFF
    app_binarr = app.tobinarray()
    ihout = IntelHex()
    ihout.padding = 0xFF
    
    ihout.frombytes(app_binarr, 0x8007400)
    ihout.frombytes(pack('<hhI', 1, 1, getBootloaderLen(env)), 0x8007000)
    ihout.tofile(str(target[0]), format='hex')
    
env_app.Append(BUILDERS={
    'shift_bootloader': Builder(
               action=shift_bootloader,
               suffix='.hex',
               src_suffix='.hex')
})

bootloader_update_nosignature = env_app_nosignature.Program(
    target = 'bootloader_update_nosignature',
    source = [
        env_app_nosignature.Object(target = 'bootloader_update.o', source='bootloader_update.cpp'),
        env_app_nosignature.Object(target = 'CAN_bootloader_update.o', source='CAN.cpp'),
        env_app_nosignature.Object(target = 'signature_bootloader_update.o', source='signature.cpp'),
        ],
    LIBS = ['lowLevel_bootloader_update'],
    )

nosignatureHex = env_app_nosignature.elf2hex('bootloader_update_nosignature',bootloader_update_nosignature)
env_app_nosignature.Objcopy('bootloader_update_nosignature',bootloader_update_nosignature)

application = env_app.Program(
    target = 'bootloader_update',
    source = [
        'bootloader_update.o',
        'CAN_bootloader_update.o',
        'signature_bootloader_update.o',
        ],
    LIBS = ['lowLevel_bootloader_update']
    )

env_app['app_nosignature'] = nosignatureHex
env_app.AddPreAction(application, env_app['Checksum'])
env_app.Depends(application, nosignatureHex)

env_app_nosignature.Append(LINKFLAGS = [
    '-Wl,-Map={}/bootloader_update_nosignature.map,--cref'.format(Dir('.').path),
    '-Wl,--defsym=APP_LEN=0',
    '-Wl,--defsym=CALC_CRC=0',
    '-Wl,--defsym=MAJOR_VERSION=${MAJOR_VERSION}',
    '-Wl,--defsym=MINOR_VERSION=${MINOR_VERSION}',
    env['LINKER_SCRIPT_bootloader_update'],
    ])

env_app.Append(LINKFLAGS = [
    '-Wl,-Map={}/bootloader_update.map,--cref'.format(Dir('.').path),
    '-Wl,--defsym=MAJOR_VERSION=${MAJOR_VERSION}',
    '-Wl,--defsym=MINOR_VERSION=${MINOR_VERSION}',
    env['LINKER_SCRIPT_bootloader_update'],
    ])

application_hex = env_app.elf2hex('bootloader_update_application_part',application)
application_bin = env_app.Objcopy('bootloader_update_application_part',application)
shifted_bootloader_hex = env_app.shift_bootloader('bootloader_update_shifted_bootloader', env_app['bootloader_hex'])
application_bootloader_update_hex = env_app.mergehex('bootloader_update_application',
  [application_hex, shifted_bootloader_hex])
env_app.hex2bin('bootloader_update_application',application_bootloader_update_hex)

complete_hex = env_app.mergehex('bootloader_update',
  [application_bootloader_update_hex, env['bootloader_hex']])
env_app.hex2bin('bootloader_update',complete_hex)
env_app.Clean(application,'{}/bootloader_update.map'.format(Dir('.').path))

