#include <lowLevel/InternalFlash.hpp>
#include <lowLevel/applicationHook.h>
#include <lowLevel/LowLevel.hpp>

#include "signature.hpp"
#include "CAN.hpp"
#include <crc/CRC32.hpp>

struct update_flags_t {
  uint16_t valid;
  uint16_t update;
  uint32_t bootloader_size;
};

extern void* __start_update_flags_region;
extern void* __start_bootloader_update_region;

const void* update_flags_vp = &__start_update_flags_region;
const void* bootloader_update = &__start_bootloader_update_region;

void startApplication()
{
    const update_flags_t* update_flags = reinterpret_cast<const update_flags_t*>(update_flags_vp);
  
    if (update_flags->valid && update_flags->update)
    {
        lowLevel::InternalFlash::erase(reinterpret_cast<void*>(
          lowLevel::InternalFlash::BOOTLOADER_START_ADDR), lowLevel::InternalFlash::BOOTLOADER_SIZE);

        lowLevel::InternalFlash::write(reinterpret_cast<void*>( 
            lowLevel::InternalFlash::BOOTLOADER_START_ADDR), 
            reinterpret_cast<const uint8_t*>(bootloader_update), update_flags->bootloader_size);

        union {
            update_flags_t reset_flags;
            uint8_t reset_flags8[sizeof(update_flags_t)];
        };

        reset_flags = {0,0,0};
        uint32_t address = reinterpret_cast<uint32_t>(&__start_update_flags_region) - lowLevel::InternalFlash::FLASH_BASE_ADDR;
        lowLevel::InternalFlash::write(reinterpret_cast<void*>(address),reset_flags8, sizeof(reset_flags8));
    }
  
    //restart into update mode
    lowLevel::LowLevel::bootflags(application::CAN::AVAILABLE::UPDATE_MODE);
    uint32_t bootflags = lowLevel::LowLevel::bootflags();
    lowLevel::LowLevel::bootflagCRC(CRC32::compute(reinterpret_cast<uint8_t*>(&bootflags), 
        sizeof(decltype(bootflags))));
    
    for (uint32_t i = 0; i < 10000; i++)
    {
        asm("NOP");
    }
    
    lowLevel::LowLevel::reset();
}
