#ifndef __APPLICATION_SIGNATURE_HPP__
#define __APPLICATION_SIGNATURE_HPP__

//std
#include <cstdint>

//lib

//local

namespace signature
{  
    extern uint16_t major_version;
    extern uint16_t minor_version;

    struct signature_t
    {
        uint32_t checksum;
        uint32_t app_len;
        uint16_t major_version;
        uint16_t minor_version;
    };
    
    extern const signature_t* signature;
}

#endif //__APPLICATION_SIGNATURE_HPP__
