#ifndef __CANTEMPERATURESENSORS_HPP__
#define __CANTEMPERATURESENSORS_HPP__


#include <lowLevel/TemperatureSensors.hpp>
#include "CAN.hpp"

#include <message/Receiver.hpp>
#include <message/Message.hpp>

#include "MainQueue.hpp"
namespace CANTemperatureSensors
{
    
class Receiver : public message::Receiver<int>
{
public:
    Receiver(message::ReceiverQueue& q) : message::Receiver<int>(q) {}
    virtual void receive(message::Message<int>&) final override;
    void init();
};

}

#endif //__CANTEMPERATURESENSORS_HPP__
