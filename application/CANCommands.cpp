//std
#include <chrono>

//lib
#include <crc/CRC32.hpp>
#include <lowLevel/LowLevel.hpp>

//local
#include "CANCommands.hpp"
#include "CAN.hpp"
#include "config.hpp"
#include "signature.hpp"

uint32_t application::cancommands::device_id()
{
    return application::CAN::device_id(config::config[config::active_config]->device_id,
        config::config[config::active_config]->device_type);
}

uint32_t application::cancommands::device_type()
{
    return config::config[config::active_config]->device_type;
}

uint32_t application::cancommands::device_id(uint32_t device_type)
{
    return application::CAN::device_id(config::config[config::active_config]->device_id,
        device_type);
}

void application::cancommands::commands(lowLevel::CAN::can_id_t& id, 
    lowLevel::CAN::data_t& data, bool remote_request)
{
    if ((id.msg_id == application::CAN::CAN_MSG_ID::REQUEST_PARAMETER) && remote_request)
    {
        const uint8_t request_id[] {
            application::CAN::CAN_MSG_ID::APPLICATION_VERSION,
            application::CAN::CAN_MSG_ID::DEVICE_UID0,
            application::CAN::CAN_MSG_ID::DEVICE_UID1,
            application::CAN::CAN_MSG_ID::CUSTOM_STRING,
            application::CAN::CAN_MSG_ID::UPTIME,
            application::CAN::CAN_MSG_ID::BAUDRATE
        
        };
        
        for (unsigned int i = 0 ; i < sizeof(request_id); i++)
        {
            lowLevel::CAN::can_id_t newid = id;
            newid.msg_id = request_id[i];
            application::cancommands::commands(newid, data, remote_request);
        }
    }
    
    if ((id.msg_id == application::CAN::CAN_MSG_ID::BAUDRATE) && remote_request)
    {
        lowLevel::CAN::data_t canData;
        canData.push_back(config::config[config::active_config]->baudrate);
        application::CAN::send(application::CAN::CAN_MSG_ID::BAUDRATE, canData, device_id());
    }
    
    if ((id.msg_id == application::CAN::CAN_MSG_ID::AVAILABLE) && remote_request)
    {
        // device availability
        lowLevel::CAN::data_t canData;
        canData.push_back(application::cancommands::application_id);
        application::CAN::send(application::CAN::CAN_MSG_ID::AVAILABLE, canData, device_id());
    }
    else if ((id.msg_id == application::CAN::CAN_MSG_ID::DEVICE_ERROR) && remote_request)
    {
        // device error
        lowLevel::CAN::data_t canData;
        application::CAN::send(application::CAN::CAN_MSG_ID::DEVICE_ERROR, canData, device_id());
    }
    else if (id.msg_id == application::CAN::CAN_MSG_ID::RESTART && !remote_request)
    {      
        // restart device
        if (data[0] == application::CAN::AVAILABLE::UPDATE_MODE)
        {            
            //restart into update mode
            lowLevel::LowLevel::bootflags(application::CAN::AVAILABLE::UPDATE_MODE);
            uint32_t bootflags = lowLevel::LowLevel::bootflags();
            lowLevel::LowLevel::bootflagCRC(CRC32::compute(reinterpret_cast<uint8_t*>(&bootflags), 
                sizeof(decltype(bootflags))));
        }
        else
        {
            lowLevel::LowLevel::bootflags(0);
            lowLevel::LowLevel::bootflagCRC(0);
        }
        
        for (uint32_t i = 0; i < 10000; i++)
        {
            asm("NOP");
        }
        
        lowLevel::LowLevel::reset();
    }
    else if (id.msg_id == application::CAN::CAN_MSG_ID::DEVICE_UID0 && remote_request)
    {
        // device id
        auto cpuid = lowLevel::LowLevel::getID();
        lowLevel::CAN::data_t canData;
        for (auto i = 0; i <= 7;i++)
        {
            canData.push_back(cpuid[i]);
        }
        application::CAN::send(application::CAN::CAN_MSG_ID::DEVICE_UID0, canData, device_id());
    }
    else if (id.msg_id == application::CAN::CAN_MSG_ID::DEVICE_UID1 && remote_request)
    {
        // device id
        auto cpuid = lowLevel::LowLevel::getID();
        lowLevel::CAN::data_t canData;
        for (auto i = 8; i <= 11;i++)
        {
            canData.push_back(cpuid[i]);
        }
        canData.push_back(0);
        canData.push_back(0);
        canData.push_back(0);
        canData.push_back(0);
        application::CAN::send(application::CAN::CAN_MSG_ID::DEVICE_UID1, canData, device_id());
    }

    else if ((id.msg_id == application::CAN::CAN_MSG_ID::DEVICE_ID_TYPE) && remote_request)
    {
        // device type
        lowLevel::CAN::data_t canData;
        canData.push_back(config::config[config::active_config]->device_id);
        canData.push_back(config::config[config::active_config]->device_type);
        application::CAN::send(application::CAN::CAN_MSG_ID::DEVICE_ID_TYPE, canData, device_id());
    }
    else if ((id.msg_id == application::CAN::CAN_MSG_ID::DEVICE_GROUP) && remote_request)
    {
        // device group
        lowLevel::CAN::data_t canData;
        for (unsigned int i = 0; i < config::CONFIG_GROUP_COUNT; i++)
        {
            canData.push_back(config::config[config::active_config]->group[i]);
        }
        application::CAN::send(application::CAN::CAN_MSG_ID::DEVICE_GROUP, canData, device_id());
    }
    else if ((id.msg_id == application::CAN::CAN_MSG_ID::CUSTOM_STRING) && remote_request)
    {
        // custom string
        lowLevel::CAN::data_t canData;
        for (unsigned int i = 0; i < config::CONFIG_CUSTOM_STRING_COUNT; i++)
        {
            canData.push_back(config::config[config::active_config]->custom_string[i]);
        }
        application::CAN::send(application::CAN::CAN_MSG_ID::CUSTOM_STRING, canData, device_id());
    }
    else if ((id.msg_id == application::CAN::CAN_MSG_ID::UPTIME) && remote_request)
    {
        // uptime
        lowLevel::CAN::data_t canData;
        union {
            unsigned int uptime;
            uint8_t uptime8[4];
        };
        
        auto uptime_auto = std::chrono::duration_cast<std::chrono::minutes>(
              std::chrono::system_clock::now().time_since_epoch()).count();
        
        uptime = static_cast<decltype(uptime)>(uptime_auto);
        
        canData.push_back(uptime8[0]);
        canData.push_back(uptime8[1]);
        canData.push_back(uptime8[2]);
        canData.push_back(uptime8[3]);
        
        application::CAN::send(application::CAN::CAN_MSG_ID::UPTIME, canData, device_id());
    }
    else if ((id.msg_id == application::CAN::CAN_MSG_ID::APPLICATION_VERSION) && remote_request)
    {
        // get version
        lowLevel::CAN::data_t canData;
        canData.push_back(signature::signature->major_version >> 8);
        canData.push_back(signature::signature->major_version & 0xFF);
        canData.push_back(signature::signature->minor_version >> 8);
        canData.push_back(signature::signature->minor_version & 0xFF);
        application::CAN::send(application::CAN::CAN_MSG_ID::APPLICATION_VERSION, canData, device_id());
    }
}
