#ifndef __APPLICATION_CANCOMMANDS_HPP__
#define __APPLICATION_CANCOMMANDS_HPP__

//std
#include <cstdint>

//lib

//local
#include <lowLevel/CAN.hpp>

namespace application
{
    namespace cancommands
    {    
        extern uint8_t application_id;
        void commands(lowLevel::CAN::can_id_t& id, lowLevel::CAN::data_t& data, 
            bool remote_request);
        uint32_t device_id();
        uint32_t device_id(uint32_t device_type);
        uint32_t device_type();
    }
}

#endif //__APPLICATION_CANCOMMANDS_HPP__
