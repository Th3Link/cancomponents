#ifndef __APPLICATION_CAN_HPP__
#define __APPLICATION_CAN_HPP__

//std
#include <etl/vector.h>
#include <cstdint>

//lib

//local
#include <lowLevel/CAN.hpp>

namespace application
{
    namespace CAN
    {
        namespace CAN_ERROR
        {
            constexpr uint8_t FLASH_OVERRUN = 0x1;
            constexpr uint8_t NO_CONFIG = 0x2;
            constexpr uint8_t DEVICE_ID_TYPE_ERROR = 0x3;
            constexpr uint8_t FIRMWARE_CORRUPT = 0x4;
        }

        namespace CAN_MSG_ID
        {
            constexpr uint8_t AVAILABLE = 0;
            constexpr uint8_t DEVICE_ERROR = 1;
            constexpr uint8_t RESTART = 2;
            constexpr uint8_t DEVICE_UID0 = 3;
            constexpr uint8_t DEVICE_UID1 = 4;
            constexpr uint8_t DEVICE_ID_TYPE = 5;
            constexpr uint8_t DEVICE_GROUP = 6;
            constexpr uint8_t APPLICATION_VERSION = 7;
            constexpr uint8_t BAUDRATE = 8;
            constexpr uint8_t UPTIME = 9;
            constexpr uint8_t CUSTOM_STRING = 10;
            constexpr uint8_t PWM_FREQUENCY = 11;
            constexpr uint8_t REQUEST_PARAMETER = 12;
            constexpr uint8_t FLASH_SELECT = 16;
            constexpr uint8_t FLASH_ERASE = 17;
            constexpr uint8_t FLASH_READ = 18;
            constexpr uint8_t FLASH_WRITE = 19;
            constexpr uint8_t FLASH_VERIFY = 20;
            constexpr uint8_t BUTTON_EVENT = 30;
            constexpr uint8_t TEMPERATURE_SENSOR = 31;
            constexpr uint8_t LAMP = 90;
            constexpr uint8_t LAMP_STATE = 58;
            constexpr uint8_t LAMP_GROUP = 90;
            constexpr uint8_t PIR_SENSOR = 128;
            constexpr uint8_t HUMIDITY_SENSOR = 129;
            constexpr uint8_t RELAIS = 130;
            constexpr uint8_t RELAIS_STATE = 131;
            constexpr uint8_t ROLLERSHUTTER = 132;
            constexpr uint8_t ROLLERSHUTTER_STATE = 133;
            constexpr uint8_t ROLLERSHUTTER_MODE = 134;
            //30      button reading
            //31      temperature reading
            //32...56 get lamp state
            //58      get lamp group state
            //64...88 set lamp command
            //90      set lamp group command
            //91      get lamp state. returns the highest pwm value
        }
        
        namespace AVAILABLE
        {
            constexpr uint8_t NOT_READY = 0;
            constexpr uint8_t APPLICATION = 1;
            constexpr uint8_t UPDATE_MODE = 2;
        }
        
        namespace BUTTON_EVENT // button no in 0
        {
            constexpr uint8_t PRESSED = 0;
            constexpr uint8_t RELEASED = 1;
            constexpr uint8_t HOLD = 2; //hold counter in 3
        }
        
        namespace ID
        {
            constexpr uint32_t TemperatureSensors = 31;
        }
        namespace Device
        {
            constexpr uint32_t Sensor = 1000000;
            constexpr uint32_t Lamp = 2000000;
            constexpr uint32_t Button = 3000000;
            constexpr uint32_t Relais = 4000000;
            constexpr uint32_t TemperatureSensors = 5000000;
            constexpr uint32_t TemperatureSensorsNG = 0x0100;
            constexpr uint32_t RelaisNG = 0x0200;
            constexpr uint32_t Lamps = 0x0300;
            constexpr uint32_t ButtonNG = 0x0400;
        }
        
        constexpr uint32_t device_id(uint8_t device_id, uint8_t device_type)
        {
            return device_id << 8 |
                device_type << 16 |
                0x10000000;
        }
        
        struct data_t
        {
                uint8_t messageId;
                uint8_t data[8];
                uint8_t data_len;
        };
        
        void send(uint32_t messageId, lowLevel::CAN::data_t& data, uint32_t device);
    }
}

#endif //__APPLICATION_CAN_HPP__
