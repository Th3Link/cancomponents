#ifndef __APPLICATION_CANSSR_HPP__
#define __APPLICATION_CANSSR_HPP__

//std

//lib

//local
#include <lowLevel/CAN.hpp>

namespace application
{
    void init(void);
}

#endif //__APPLICATION_CANSSR_HPP__
