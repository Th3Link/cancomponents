#ifndef __CANRELAIS_HPP__
#define __CANRELAIS_HPP__


#include <lowLevel/Relais.hpp>
#include "CAN.hpp"

#include <message/Receiver.hpp>
#include <message/Message.hpp>

#include "MainQueue.hpp"

/**
 * this struct is used to track actions on every rollershutter, since automatic switch
 * of is mandatory. A too fast up-stop-down requence would stop the last down movement
 * with the automatic stop from the up action
 */
struct rollershutter_action_t
{
    uint8_t number;
    uint32_t action;
};

class CANReceiverReceiver : 
    public message::Receiver<application::CAN::data_t>, 
    public message::Receiver<rollershutter_action_t>
{    
public:
    void init();
    CANReceiverReceiver(message::ReceiverQueue& q) : 
        message::Receiver<application::CAN::data_t>(q), 
        message::Receiver<rollershutter_action_t>(q) {}
    virtual void receive(message::Message<application::CAN::data_t>&) final override;
    virtual void receive(message::Message<rollershutter_action_t>&) final override;
    bool setRollershutter(uint8_t number, uint8_t state, uint32_t time);
    void sendRollershutter(uint8_t number);
};

#endif //__CANRELAIS_HPP__
