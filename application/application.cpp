#include <message/Receiver.hpp>
#include <message/Message.hpp>
#include <message/Queue.hpp>

#include <lowLevel/applicationHook.h>
#include <lowLevel/LowLevel.hpp>
#include <lowLevel/CAN.hpp>

#include "CAN.hpp"
#include "MainQueue.hpp"
#include "application.hpp"
#include "deviceId.hpp"
#include "config.hpp"

class KeepAlive : public message::Receiver<int>
{
public:
    KeepAlive(message::ReceiverQueue& q) : message::Receiver<int>(q)
    {
        
    }
    virtual void receive(message::Message<int>& m) final override
    {
        lowLevel::LowLevel::keepAlive();
        message::Message<int>::send(queue, *this, message::Event::UPDATE, 
            std::move(m.data), std::chrono::milliseconds(500));
    }
};

// put queue and receiver to static data space
MainQueue::queue_t MainQueue::queue;
InterruptQueue::queue_t InterruptQueue::queue;

void startApplication()
{
    //initialize lowlevel
    lowLevel::LowLevel::init();
        
    // set active and write config. do not react to missing config since this
    // is already done by the bootloader which does not start the application
    // without a valid config
    config::init();
    
    lowLevel::CAN::init(config::config[config::active_config]->baudrate);
    
    lowLevel::CAN::setFilter(
        config::config[config::active_config]->device_id, 
        config::config[config::active_config]->device_type,
        config::config[config::active_config]->group);
    
    application::init();
    
    KeepAlive keepAlive(MainQueue::queue);
    message::Message<int>::send(MainQueue::queue, keepAlive, message::Event::UPDATE, 
        0, std::chrono::milliseconds(500));
    
    while(1) {
        //interupts aus
        MainQueue::queue.dispatch();
        InterruptQueue::queue.dispatch();
        //intereupts an
    }
}
