#ifndef __APPLICATION_CONFIG_HPP__
#define __APPLICATION_CONFIG_HPP__

//std
#include <cstdint>

//lib

//local

namespace config
{  
    constexpr uint32_t CONFIG_GROUP_COUNT = 6;
    constexpr uint32_t CONFIG_CUSTOM_STRING_COUNT = 8;
    
    // only append, otherwise the compalibly copy will do weird stuff;
    // however, you can fix it by redoing the config
    #pragma pack(push,1)
    struct config_t
    {
        uint32_t checksum;
        uint32_t size;
        uint32_t write_count;
        uint8_t device_id;
        uint8_t device_type;
        uint8_t group[CONFIG_GROUP_COUNT];
        uint8_t baudrate;
        uint8_t custom_string[CONFIG_CUSTOM_STRING_COUNT];
        uint16_t pwm_frequency;
    };
    #pragma pack(pop)
    
    constexpr uint32_t config_t_data_size = sizeof(config_t)-2*sizeof(uint32_t);
    
    extern uint8_t active_config;
    extern uint8_t write_config;
    extern const config_t* config[3];
    extern config::config_t emergency_config;
    bool init();
    void write(config::config_t);
}

#endif //__APPLICATION_CONFIG_HPP__
