//std
#include <algorithm>

//lib

//local
#include "CANRelais.hpp"
#include "CANCommands.hpp"

/*
 * How do we encode switch on and off and a time?
 * 8 bytes
 */
constexpr auto UNBLOCK_TIME = std::chrono::milliseconds(500);
constexpr auto REQUEUE_TIME = std::chrono::milliseconds(600);

#pragma pack(push,1)
struct can_relais_message_t
{
    union
    {
        uint8_t data[8];
        struct
        {
            uint32_t number : 8;
            uint32_t state : 8;
            uint32_t time : 24;
            uint32_t reserved : 24;
        };
    };
};
#pragma pack(pop)

enum rollershutter_state_t {
    MOVING, STOP, BLOCKED
};

static std::array<rollershutter_state_t, 6> states {
  rollershutter_state_t::STOP,
  rollershutter_state_t::STOP,
  rollershutter_state_t::STOP,
  rollershutter_state_t::STOP,
  rollershutter_state_t::STOP,
  rollershutter_state_t::STOP
  };
static std::array<uint32_t, 6> actions { 0 };

void CANReceiverReceiver::init()
{
    
}

void CANReceiverReceiver::receive(message::Message<rollershutter_action_t>& m)
{
    if (m.event == message::Event::STOP_TIME)
    {
        if (m.data.action >= actions[m.data.number])
        {
            //only execute stop when still the last action
            setRollershutter(m.data.number, 0, 0);
        }
    }
    else if (m.event == message::Event::BLOCK_TIME)
    {
        setRollershutter(m.data.number, 3, 0);
        sendRollershutter(m.data.number);
    }
}

void CANReceiverReceiver::receive(message::Message<application::CAN::data_t>& m)
{
    can_relais_message_t crm {0};
    
    for (unsigned int i = 0; i < m.data.data_len; i++)
    {
        crm.data[i] = m.data.data[i];
    }

    if (m.event == message::Event::CAN_ROLLERSHUTTER_SET)
    {
        if (!setRollershutter(crm.number, crm.state, crm.time))
        {
            //requeue
            message::Message<application::CAN::data_t>::send(MainQueue::queue, *this, message::Event::CAN_ROLLERSHUTTER_SET, 
                std::move(m.data), REQUEUE_TIME);
            return;
        }
        return;
        
    }
    else if (m.event == message::Event::CAN_ROLLERSHUTTER_GET)
    {
        CANReceiverReceiver::sendRollershutter(m.data.messageId);
        return;
    }
    
    if (m.event == message::Event::CAN_RELAIS_SET)
    {       
        lowLevel::Relais::set(crm.number, crm.state);
        /*
         * invert switch after timeout time 
         */
        if (crm.time > 0)
        {
            can_relais_message_t delayed_crm;
            delayed_crm.number = crm.number;
            delayed_crm.state = !crm.state;
            delayed_crm.time = 0;
            
            application::CAN::data_t data;
            data.messageId = m.data.messageId;
            data.data_len = sizeof(can_relais_message_t);
            for (unsigned int i = 0; i < sizeof(can_relais_message_t); i++)
            {
                data.data[i] = delayed_crm.data[i];
            }
            
            // queue timeout message in
            message::Message<application::CAN::data_t>::send(MainQueue::queue, *this, message::Event::CAN_RELAIS_SET, 
                std::move(data), std::chrono::milliseconds(crm.time));
        }
    }
    
    // send state notification anyway
    bool state = lowLevel::Relais::get(crm.number);
    lowLevel::CAN::data_t data;
    data.push_back(state ? 1 : 0);
    application::CAN::send(m.data.messageId + 32, data,
        application::cancommands::device_id());
}

void CANReceiverReceiver::sendRollershutter(uint8_t number)
{
    bool down = lowLevel::Relais::get(number * 2);
    bool up = lowLevel::Relais::get(number * 2 + 1);
    
    lowLevel::CAN::data_t data;
    
    data.push_back(number);
    
    if (!down && !up)
    {
        data.push_back(0);
    }
    else if (down && !up)
    {
        data.push_back(1);
    }
    else if (!down && up)
    {
        data.push_back(2);
    }
    else
    {
        data.push_back(0xFF);
        // system broken... rollershutter burning or fuse definitly destroyed
    }
    data.push_back(0); //time
    data.push_back(0); //time
    data.push_back(0); //time
    data.push_back(0); //bank
    application::CAN::send(133, data, application::cancommands::device_id());
}

bool CANReceiverReceiver::setRollershutter(uint8_t number, uint8_t state, uint32_t time)
{

    auto hwoff = !lowLevel::Relais::get(number * 2) && !lowLevel::Relais::get(number * 2 + 1);
     
    if (state == 0)
    {
        lowLevel::Relais::set(number * 2, 0); //(0=0,1=2,2=4,3=6,4=8,5=10)
        lowLevel::Relais::set(number * 2 + 1, 0);
        if (states[number] != rollershutter_state_t::STOP)
        {
            states[number] = rollershutter_state_t::BLOCKED;
            message::Message<rollershutter_action_t>::send(MainQueue::queue, *this, message::Event::BLOCK_TIME, 
                {number, 0}, UNBLOCK_TIME);
        }
    }
    else if (state == 1 || state == 2)
    {
        // go up
        if ((states[number] == rollershutter_state_t::STOP) && hwoff)
        {
            lowLevel::Relais::set(number * 2, 2 - state); //(0=0,1=2,2=4,3=6,4=8,5=10)
            lowLevel::Relais::set(number * 2 + 1, state - 1);
            states[number] = rollershutter_state_t::MOVING;
            actions[number]++;
            message::Message<rollershutter_action_t>::send(MainQueue::queue, *this, message::Event::STOP_TIME, 
                {number, actions[number]}, std::chrono::milliseconds(time));
            sendRollershutter(number);
        }
        else
        {
            setRollershutter(number, 0, 0);
            return false;
        }
    }
    else if (state == 3)
    {
        // unblock
        if (hwoff)
        {
            states[number] = rollershutter_state_t::STOP;
        }
        else
        {
            lowLevel::Relais::set(number * 2, 0); //(0=0,1=2,2=4,3=6,4=8,5=10)
            lowLevel::Relais::set(number * 2 + 1, 0);
            message::Message<rollershutter_action_t>::send(MainQueue::queue, *this, message::Event::BLOCK_TIME, 
                {number, 0}, UNBLOCK_TIME);
        }
    }
    return true;
}
