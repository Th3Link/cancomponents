#include <lowLevel/CAN.hpp>
#include <lowLevel/Relais.hpp>
#include <lowLevel/TemperatureSensors.hpp>
#include <lowLevel/LowLevel.hpp>

#include "application.hpp"
#include "CANTemperatureSensors.hpp"
#include "CANRelais.hpp"
#include "CANCommands.hpp"
#include "CAN.hpp"
#include "config.hpp"

uint8_t application::cancommands::application_id = 0x1;

CANTemperatureSensors::Receiver canTemperatureSensor(MainQueue::queue);
CANReceiverReceiver canRelais(MainQueue::queue);

class CANSSR :
    public message::Receiver<int>
{    
public:
    void init()
    {
        
    }
    
    CANSSR(message::ReceiverQueue& q) :
        message::Receiver<int>(q) {}
    virtual void receive(message::Message<int>& m) final override
    {
        if (m.event == message::Event::CAN_NOTIFY)
        {
            lowLevel::CAN::can_id_t id;
            lowLevel::CAN::data_t data;
            bool remote_request;
            while (lowLevel::CAN::receive(id, data, remote_request))
            {
                if ((id.msg_id == application::CAN::CAN_MSG_ID::RELAIS_STATE) && remote_request)
                {
                    // get relais state
                    application::CAN::data_t aData;
                    aData.messageId = application::CAN::CAN_MSG_ID::RELAIS_STATE;
                    aData.data_len = data.size();
                    for (unsigned int i=0; i < aData.data_len;i++)
                    {
                        aData.data[i] = data[i];
                    }
                    message::Message<application::CAN::data_t>::send(MainQueue::queue, 
                        canRelais, message::Event::CAN_RELAIS_GET, std::move(aData));
                }
                else if ((id.msg_id == application::CAN::CAN_MSG_ID::RELAIS) && !remote_request)
                {
                    // set relais state
                    application::CAN::data_t aData;
                    aData.messageId = application::CAN::CAN_MSG_ID::RELAIS;
                    aData.data_len = data.size();
                    for (unsigned int i=0; i < aData.data_len;i++)
                    {
                        aData.data[i] = data[i];
                    }
                    message::Message<application::CAN::data_t>::send(MainQueue::queue, 
                        canRelais, message::Event::CAN_RELAIS_SET, std::move(aData));
                }
                else if ((id.msg_id == application::CAN::CAN_MSG_ID::ROLLERSHUTTER) && !remote_request)
                {
                    // set rollershutter state
                    application::CAN::data_t aData;
                    aData.messageId = application::CAN::CAN_MSG_ID::ROLLERSHUTTER;
                    aData.data_len = data.size();
                    for (unsigned int i=0; i < aData.data_len;i++)
                    {
                        aData.data[i] = data[i];
                    }
                    message::Message<application::CAN::data_t>::send(MainQueue::queue, 
                        canRelais, message::Event::CAN_ROLLERSHUTTER_SET, std::move(aData));
                }
                else if ((id.msg_id == application::CAN::CAN_MSG_ID::ROLLERSHUTTER_STATE) && remote_request)
                {
                    // get rollershutter state
                    application::CAN::data_t aData;
                    aData.messageId = application::CAN::CAN_MSG_ID::ROLLERSHUTTER_STATE;
                    aData.data_len = data.size();
                    for (unsigned int i=0; i < aData.data_len;i++)
                    {
                        aData.data[i] = data[i];
                    }
                    message::Message<application::CAN::data_t>::send(MainQueue::queue, 
                        canRelais, message::Event::CAN_ROLLERSHUTTER_GET, std::move(aData));
                }
                else
                {
                    application::cancommands::commands(id, data, remote_request);
                }
                data.clear();
            }
        }
    }
};

CANSSR canssr(MainQueue::queue);

void application::init()
{
    //reset bootflags
    lowLevel::LowLevel::bootflags(0);
    lowLevel::LowLevel::bootflagCRC(0);
    
    //initialize lowLevel
    lowLevel::TemperatureSensors::init();
    lowLevel::Relais::init();
    
    //initialize application
    canTemperatureSensor.init();
    canRelais.init();
    canssr.init();
    
    lowLevel::CAN::data_t data;
    data.push_back(application::CAN::AVAILABLE::APPLICATION);
    application::CAN::send(application::CAN::CAN_MSG_ID::AVAILABLE, data, 
        application::cancommands::device_id());
}

/*
 * This call comes from in interrupt. Sync with main thread using a message.
 * In this way also dispatch the can messages to thier destination
 */
void lowLevel::CAN::notify()
{
    message::Message<int>::send(InterruptQueue::queue, 
        canssr, message::Event::CAN_NOTIFY, 0);
}
