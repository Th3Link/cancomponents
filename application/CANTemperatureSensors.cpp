
//std
#include <algorithm>
#include <etl/vector.h>

//lib

//local
#include "CANTemperatureSensors.hpp"
#include <lowLevel/TemperatureSensors.hpp>
#include "CAN.hpp"
#include "CANCommands.hpp"

/*
 * How do we encode switch on and off and a time?
 * 8 bytes
 */

enum MeasuringStates
{
    Convert,
    Read
};

struct can_temperature_sensor_message_t
{
    union
    {
        uint8_t data[8];
        lowLevel::TemperatureSensors::Message temperatureMessage;
    };
};

void CANTemperatureSensors::Receiver::receive(message::Message<int>& m)
{
    
    if (m.data == MeasuringStates::Convert)
    {
        // start conversion. ignore readback bit and queue in safe 2 secons
        // (750ms worst case convesion time according to the datasheet)
        lowLevel::TemperatureSensors::startConversion();
        message::Message<int>::send(MainQueue::queue, *this, message::Event::UPDATE, 
                MeasuringStates::Read, std::chrono::seconds(2));
    }
    else if (m.data == MeasuringStates::Read)
    {
        etl::vector<lowLevel::TemperatureSensors::Message, 
            lowLevel::TemperatureSensors::MAX> temperatures;
        lowLevel::TemperatureSensors::pollSensors(temperatures);
        
        // etl::vector is like an array with indexing whit is in there, so
        // so we can easily iterate over it 
        for (auto& temperature : temperatures)
        {
            can_temperature_sensor_message_t msg;
            msg.temperatureMessage = temperature;
            
            lowLevel::CAN::data_t canData;
            for (auto& c : msg.data)
            {
                canData.push_back(c);
            }
                
            application::CAN::send(application::CAN::CAN_MSG_ID::TEMPERATURE_SENSOR, canData,
                application::cancommands::device_id());
        }
        message::Message<int>::send(MainQueue::queue, *this, message::Event::UPDATE, 
                MeasuringStates::Convert, std::chrono::seconds(300));
    }
}

void CANTemperatureSensors::Receiver::init()
{
    // start with start conversion
    message::Message<int>::send(MainQueue::queue, *this, message::Event::UPDATE, 
            MeasuringStates::Convert, std::chrono::seconds(5));
}
