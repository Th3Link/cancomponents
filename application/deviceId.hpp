#ifndef __APPLICATION_DEVICE_ID_HPP__
#define __APPLICATION_DEVICE_ID_HPP__

//std

//lib

//local

namespace application
{
    extern uint32_t device_id;
}

#endif //__APPLICATION_DEVICE_ID_HPP__
