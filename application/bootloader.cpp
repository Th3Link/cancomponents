#include <lowLevel/CAN.hpp>
#include <lowLevel/InternalFlash.hpp>
#include <lowLevel/applicationHook.h>
#include <lowLevel/LowLevel.hpp>

#include <crc/CRC32.hpp>
#include <message/Atomic.hpp>

#include "CAN.hpp"
#include "config.hpp"
#include "signature.hpp"
#include "CANCommands.hpp"

static bool selected_by_cpu_id = true;
uint8_t application::cancommands::application_id = 0x2;

bool checkApplication();
bool checkBootflags();
inline void flashCommands(lowLevel::CAN::can_id_t&, lowLevel::CAN::data_t&, bool);
inline void bootloaderOnlyCommands(lowLevel::CAN::can_id_t&, lowLevel::CAN::data_t&, bool);

struct sync_t
{
    lowLevel::CAN::can_id_t id;
    lowLevel::CAN::data_t data; 
    bool remote_request;
};

volatile sync_t sync;
volatile bool sync_needed = false;


void startApplication()
{
    bool configSuccess = config::init();
    
    lowLevel::CAN::init(config::config[config::active_config]->baudrate);
    
    bool halt = false;
    lowLevel::CAN::setFilter(
        config::config[config::active_config]->device_id, 
        config::config[config::active_config]->device_type,
        config::config[config::active_config]->group);
    
    if (!configSuccess)
    {
        selected_by_cpu_id = false;
        lowLevel::CAN::data_t data;
        data.push_back(application::CAN::CAN_ERROR::NO_CONFIG);
        application::CAN::send(application::CAN::CAN_MSG_ID::DEVICE_ERROR, data, 
            application::cancommands::device_id());
    }
    
    if (!configSuccess || checkBootflags() || checkApplication())
    {
        //endless loop until the device gets restarted. otherwise the
        //main function in lowlevel start the application

        lowLevel::CAN::data_t data1;
        data1.push_back(application::CAN::AVAILABLE::UPDATE_MODE);
        application::CAN::send(application::CAN::CAN_MSG_ID::AVAILABLE, data1, 
            application::cancommands::device_id());

        lowLevel::CAN::data_t canData;
        canData.push_back(signature::signature->major_version >> 8);
        canData.push_back(signature::signature->major_version & 0xFF);
        canData.push_back(signature::signature->minor_version >> 8);
        canData.push_back(signature::signature->minor_version & 0xFF);
        application::CAN::send(application::CAN::CAN_MSG_ID::APPLICATION_VERSION, canData, 
          application::cancommands::device_id());

        halt = true;
    }
    
    uint32_t cyclecount = 0;
    while (cyclecount > 1000 || halt)
    {
        lowLevel::CAN::can_id_t id;
        lowLevel::CAN::data_t data; 
        bool remote_request;
        while (lowLevel::CAN::receive(id, data, remote_request))
        {
            flashCommands(id, data, remote_request);
            application::cancommands::commands(id, data, remote_request);
            bootloaderOnlyCommands(id, data, remote_request);
            data.clear();
        }
        cyclecount++;
    }
}

bool checkApplication()
{
    if (signature::signature->app_len > lowLevel::InternalFlash::APPLICATION_MAX_SIZE)
    {
        return false;
    }
    
    auto checksum = CRC32::compute(lowLevel::InternalFlash::APPLICATION_START_PTR, 
        signature::signature->app_len);
    
    if (checksum != signature::signature->checksum)
    {
        // switch to update mode
        lowLevel::CAN::data_t data;
        data.push_back(application::CAN::CAN_ERROR::FIRMWARE_CORRUPT);
        application::CAN::send(application::CAN::CAN_MSG_ID::DEVICE_ERROR, data, 
            application::cancommands::device_id());
        return true;
    }
    return false;
}

bool checkBootflags()
{
    /*
     * set bootflags_crc[0] = 2
     * set bootflags_crc[1] = 0x8b4d1797 for debug
     */
    uint32_t bootflags = lowLevel::LowLevel::bootflags();    
    if ((CRC32::compute(reinterpret_cast<uint8_t*>(&bootflags), sizeof(decltype(bootflags))) == 
        lowLevel::LowLevel::bootflagCRC()) && 
        (bootflags == application::CAN::AVAILABLE::UPDATE_MODE))
    {
        // switch to update mode
        return true;
    }
    return false;
}

constexpr uint32_t convert(uint8_t* data, unsigned int offset = 0)
{
    return data[0+offset] << 24 | data[1+offset] << 16 | data[2+offset] << 8 | data[3+offset];
}

/*
 * This call comes from in interrupt. Sync with main thread using a message.
 * In this way also dispatch the can messages to thier destination
 */
void lowLevel::CAN::notify()
{
    // do nothing here, the bootloader only reacts on can messages and is always polling
}

void bootloaderOnlyCommands(lowLevel::CAN::can_id_t& id, lowLevel::CAN::data_t& data, 
    bool remote_request)
{
    if (id.msg_id == application::CAN::CAN_MSG_ID::DEVICE_UID0 && !remote_request)
    {
        // select device by UID0
        auto cpuid = lowLevel::LowLevel::getID();
        bool all_zero = true;
        selected_by_cpu_id = true;
        for (unsigned int i = 0; i < data.size();i ++)
        {
            if (data[i])
            {
                all_zero = false;
            }
            if (data[i] != cpuid[i])
            {
                selected_by_cpu_id = false;
            }
        }
        // select by cpu id can be overwritten by sending 8 times 0x00
        if (all_zero && (data.size() == 8))
        {
            selected_by_cpu_id = true;
        }
    }
    else if ((id.msg_id == application::CAN::CAN_MSG_ID::DEVICE_ID_TYPE) && !remote_request)
    {
        // device type
        // sanity check
        if (!selected_by_cpu_id || data.size() != 2 || !data[0] || !data[1])
        {
            lowLevel::CAN::data_t canData;
            canData.push_back(application::CAN::CAN_ERROR::DEVICE_ID_TYPE_ERROR);
            application::CAN::send(application::CAN::CAN_MSG_ID::DEVICE_ERROR, canData, application::cancommands::device_id());
            return;
        }
        
        config::config_t new_config = *config::config[config::active_config];
        
        // overwrite device_id and device_type
        new_config.device_id = data[0];
        new_config.device_type = data[1];
        
        config::write(new_config);
    }
    else if ((id.msg_id == application::CAN::CAN_MSG_ID::DEVICE_GROUP) && !remote_request)
    {
        // device group
        config::config_t new_config = *config::config[config::active_config];
        
        // overwrite group info
        for (unsigned int i = 0; i < data.size(); i++)
        {
            new_config.group[i] = data[i];
        }
        
        config::write(new_config);
    }
    else if ((id.msg_id == application::CAN::CAN_MSG_ID::BAUDRATE) && !remote_request)
    {
        // baudrate
        config::config_t new_config = *config::config[config::active_config];
        
        // overwrite
        new_config.baudrate = data[0];
        
        config::write(new_config);
    }
    else if ((id.msg_id == application::CAN::CAN_MSG_ID::CUSTOM_STRING) && !remote_request)
    {
        // custom string
        config::config_t new_config = *config::config[config::active_config];
        
        // overwrite
        for (unsigned int i = 0; i < data.size(); i++)
        {
            new_config.custom_string[i] = data[i];
        }
        
        config::write(new_config);
    }
}



void flashCommands(lowLevel::CAN::can_id_t& id, lowLevel::CAN::data_t& data, bool remote_request)
{
    static uint32_t flash_address = 0;
    static uint32_t flash_length = 0;
    static uint32_t write_address = 0;
    static uint32_t read_address = 0;
    
    if (id.msg_id == application::CAN::CAN_MSG_ID::FLASH_SELECT)
    {
        // flash select
        flash_address = convert(data.data());
        flash_length = convert(data.data(), 4);
        
        //check limits to avoid crash on wrong adresses
        if (flash_address > lowLevel::InternalFlash::APPLICATION_MAX_SIZE || flash_length < 0x800)
        {
            flash_address = 0;
            // send error notification
            lowLevel::CAN::data_t canData;
            canData.push_back(application::CAN::CAN_ERROR::FLASH_OVERRUN);
            application::CAN::send(application::CAN::CAN_MSG_ID::DEVICE_ERROR, 
                canData, application::cancommands::device_id());
            return;
        }
        
        write_address = flash_address;
        read_address = flash_address;
    }
    else if (id.msg_id == application::CAN::CAN_MSG_ID::FLASH_ERASE && !remote_request)
    {
        // flash erase remind, that always complete 1Kb pages are getting erased
        if (flash_length != 0)
        {
            lowLevel::InternalFlash::erase(reinterpret_cast<void*>(flash_address + 
                lowLevel::InternalFlash::SIGNATURE_START_ADDR), flash_length);
        }
    }
    else if ((id.msg_id == application::CAN::CAN_MSG_ID::FLASH_READ) && remote_request)
    {
        // flash read
        lowLevel::CAN::data_t canData;
        uint8_t t_data[8] {0};
        
        if (read_address > (lowLevel::InternalFlash::APPLICATION_MAX_SIZE + 1024))
        {
            // send error notification
            lowLevel::CAN::data_t canData;
            canData.push_back(application::CAN::CAN_ERROR::FLASH_OVERRUN);
            application::CAN::send(application::CAN::CAN_MSG_ID::DEVICE_ERROR, 
                canData, application::cancommands::device_id());
            return;
        }
        
        lowLevel::InternalFlash::read(reinterpret_cast<void*>(read_address + 
            lowLevel::InternalFlash::SIGNATURE_START_ADDR), t_data, 8);
        
        for (uint32_t j = 0; j < 8; j++)
        {
            canData.push_back(t_data[j]);
        }
        
        application::CAN::send(application::CAN::CAN_MSG_ID::FLASH_READ, canData, 
            application::cancommands::device_id());
        read_address += 8;
    }
    else if (id.msg_id == application::CAN::CAN_MSG_ID::FLASH_WRITE)
    {
        // flash write
        
        if (write_address > (lowLevel::InternalFlash::APPLICATION_MAX_SIZE + 1024))
        {
            // send error notification
            lowLevel::CAN::data_t canData;
            canData.push_back(application::CAN::CAN_ERROR::FLASH_OVERRUN);
            application::CAN::send(application::CAN::CAN_MSG_ID::DEVICE_ERROR, 
                canData, application::cancommands::device_id());
            return;
        }
        
        lowLevel::InternalFlash::write(reinterpret_cast<void*>(write_address + 
            lowLevel::InternalFlash::SIGNATURE_START_ADDR), 
            data.data(), data.size());
        write_address += data.size();
    }
    else if ((id.msg_id == application::CAN::CAN_MSG_ID::FLASH_VERIFY) && remote_request)
    {
        // flash verify
        union {
            uint32_t checksum;
            uint8_t checksum8[4];
        };
        
        if ((flash_length < (2 * 0x400)) || (flash_length > lowLevel::InternalFlash::APPLICATION_MAX_SIZE))
        {
            // send error notification
            lowLevel::CAN::data_t canData;
            canData.push_back(application::CAN::CAN_ERROR::FLASH_OVERRUN);
            application::CAN::send(application::CAN::CAN_MSG_ID::DEVICE_ERROR, 
                canData, application::cancommands::device_id());
            return;
        }
        checksum = CRC32::compute(lowLevel::InternalFlash::APPLICATION_START_PTR + 
            flash_address, flash_length - 0x400);
        lowLevel::CAN::data_t canData;
        for (uint32_t i = 0; i < 4; i++)
        {
            canData.push_back(checksum8[i]);
        }
        application::CAN::send(application::CAN::CAN_MSG_ID::FLASH_VERIFY, canData, 
            application::cancommands::device_id());
    }
}
