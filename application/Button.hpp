#ifndef __APPLICATION_BUTTON_HPP__
#define __APPLICATION_BUTTON_HPP__

//std
#include <etl/vector.h>
#include <cstdint>

//lib

//local
#include <lowLevel/CAN.hpp>

namespace application
{
    namespace Button
    {
        enum state_t
        {
            RELEASED,
            PRESSED,
            LONG_PRESSED
        };
    }
}
#endif //__APPLICATION_BUTTON_HPP__
