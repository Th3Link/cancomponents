#include <lowLevel/CAN.hpp>
#include <lowLevel/LowLevel.hpp>
#include <lowLevel/Sensors.hpp>
#include <lowLevel/Button.hpp>

#include "application.hpp"
#include "CAN.hpp"
#include "deviceId.hpp"

uint32_t application::device_id = application::CAN::Device::ButtonNG;

void application::init()
{
    //initialize lowLevel
    lowLevel::Sensors::init();
    if (lowLevel::Sensors::available())
    {
        //switch to sensor mode
    }
    else
    {
        //switch to button mode
        lowLevel::Button::init();
    }
    
    //initialize application
    
}

void lowLevel::CAN::notify()
{    

}
