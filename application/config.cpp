//std

//lib

//local
#include "config.hpp"
#include <lowLevel/InternalFlash.hpp>
#include <crc/CRC32.hpp>
#include <algorithm>

uint8_t config::active_config = 2;
uint8_t config::write_config = 1;

config::config_t config::emergency_config {};

const config::config_t* config::config[] = {
    reinterpret_cast<const config::config_t*>(lowLevel::InternalFlash::device_info_0), 
    reinterpret_cast<const config::config_t*>(lowLevel::InternalFlash::device_info_1), 
    &emergency_config
    };

void default_config(config::config_t& c)
{
    c.device_id = 0xFF;
    c.device_type = 0x3F;
    c.baudrate = 0;
    c.pwm_frequency = 1000;
}

bool config::init()
{
    //format has changed; apply old config. this only works when new values are appended.

    //read and check configs   
    uint32_t checksum_config[] = {
        CRC32::compute(reinterpret_cast<const uint8_t*>(&config::config[0]->write_count),
            std::min(config::config[0]->size, config_t_data_size)),
        CRC32::compute(reinterpret_cast<const uint8_t*>(&config::config[1]->write_count),
            std::min(config::config[1]->size, config_t_data_size))};

    if ((checksum_config[0] != config::config[0]->checksum) && (checksum_config[1] != config::config[1]->checksum))
    {
        config::active_config = 2;
        default_config(config::emergency_config);
        return false;
    }
    else if ((checksum_config[0] == config::config[0]->checksum) && 
        (checksum_config[1] == config::config[1]->checksum))
    {
        if (config::config[1]->write_count > config::config[0]->write_count)
        {
            config::active_config = 1;
            config::write_config = 0;
        }
        else
        {
            config::active_config = 0;
            config::write_config = 1;
        }
    }
    else if (checksum_config[1] == config::config[1]->checksum)
    {
        config::active_config = 1;
        config::write_config = 0;
    }
    else
    {
        config::active_config = 0;
    }
    return true;   
}

void config::write(config::config_t c)
{
    // create new struct
    union {
        config::config_t new_config;
        uint8_t new_config8[sizeof(config::config_t)];
    };
    
    new_config = c;
    
    new_config.write_count = config::config[config::active_config]->write_count + 1;
    new_config.checksum = CRC32::compute(
        reinterpret_cast<const uint8_t*>(&new_config.write_count),
        config::config_t_data_size);
    
    //write new size
    new_config.size = config::config_t_data_size;
    
    // erase old (currently inactive) struct. Erase complete page
    auto addr = reinterpret_cast<uint64_t>(config::config[config::write_config]) - lowLevel::InternalFlash::FLASH_BASE_ADDR;
    
    lowLevel::InternalFlash::erase(reinterpret_cast<const void*>(addr), 0x400);
    // write new config to page
    lowLevel::InternalFlash::write(reinterpret_cast<const void*>(addr), 
        new_config8, sizeof(new_config8));
    
    config::write_config = 1 - config::write_config;
}
