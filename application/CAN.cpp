//std

//lib

//local
#include <lowLevel/CAN.hpp>
#include "CAN.hpp" //application/CAN.hpp

/*
 * 
 */

void application::CAN::send(uint32_t messageId, lowLevel::CAN::data_t& data, uint32_t device)
{
    
    lowLevel::CAN::send(device + messageId, data);
}
