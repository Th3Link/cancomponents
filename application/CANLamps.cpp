#include <lowLevel/CAN.hpp>
#include <lowLevel/Lamps.hpp>
#include <lowLevel/TemperatureSensors.hpp>
#include <lowLevel/LowLevel.hpp>

#include <crc/CRC32.hpp>

#include "application.hpp"
#include "CANTemperatureSensors.hpp"
#include "CAN.hpp"
#include "CANCommands.hpp"
#include "config.hpp"

uint8_t application::cancommands::application_id = 0x1;

CANTemperatureSensors::Receiver canTemperatureSensor(MainQueue::queue);

struct can_lamps_message_t
{
    union
    {
        uint8_t data[8];
        struct
        {
            uint32_t state : 8;
            uint32_t lamp_mask : 24;
            uint32_t reserved2 : 32;
        };
    };
};

class CANLamps :
    public message::Receiver<int>
{    
public:
    void init();
    CANLamps(message::ReceiverQueue& q) :
        message::Receiver<int>(q) {}
    virtual void receive(message::Message<int>&) final override;
};

CANLamps lamps(MainQueue::queue);

void CANLamps::init()
{
    
}

void CANLamps::receive(message::Message<int>& m)
{
    if (m.event != message::Event::CAN_NOTIFY)
    {
        return;
    }
    
    lowLevel::CAN::can_id_t id;
    lowLevel::CAN::data_t data;
    bool remote_request;
    while (lowLevel::CAN::receive(id, data, remote_request))
    {
        if ((id.msg_id == application::CAN::CAN_MSG_ID::LAMP) && (data.size() == 4))
        {
            // set group command
            can_lamps_message_t clm;
            std::copy(data.begin(), data.end(), clm.data);
            
            for (unsigned int i = 0; i < 24; i++)
            {
                if (clm.lamp_mask & (1 << i))
                {
                    lowLevel::Lamps::value(i, clm.state);
                }
            }
        }
        
        if ((id.msg_id == application::CAN::CAN_MSG_ID::LAMP_STATE) || 
            (id.msg_id == application::CAN::CAN_MSG_ID::LAMP))
        {
            uint8_t max = 0;
            for (unsigned int i = 0; i < 24; i++)
            {
                auto v = static_cast<uint8_t>(lowLevel::Lamps::value(i));
                if (v > max)
                {
                    max = v;
                }
            }
            
            can_lamps_message_t clm;
            clm.state = max;
            clm.lamp_mask = 0xFFFFFF;
            
            lowLevel::CAN::data_t d;
            
            for (unsigned int i = 0; i < sizeof(can_lamps_message_t); i++)
            {
                d.push_back(clm.data[i]);
            }
            
            application::CAN::send(application::CAN::CAN_MSG_ID::LAMP_STATE, d, 
                application::cancommands::device_id());
                
        }
        else
        {
            application::cancommands::commands(id, data, remote_request);
        }
        data.clear();
    }
}

void application::init()
{
    //reset bootflags
    lowLevel::LowLevel::bootflags(0);
    lowLevel::LowLevel::bootflagCRC(0);
    
    //initialize lowLevel
    lowLevel::TemperatureSensors::init();
    lowLevel::Lamps::init(1000);
    
    //initialize application
    canTemperatureSensor.init();
    lamps.init();
        
    lowLevel::CAN::data_t data;
    data.push_back(application::CAN::AVAILABLE::APPLICATION);
    application::CAN::send(application::CAN::CAN_MSG_ID::AVAILABLE, data, 
        application::cancommands::device_id());
}

void lowLevel::CAN::notify()
{
    message::Message<int>::send(InterruptQueue::queue, 
        lamps, message::Event::CAN_NOTIFY, 0);
}
