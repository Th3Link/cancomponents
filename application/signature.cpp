//std

//lib

//local
#include "signature.hpp"
#include <lowLevel/InternalFlash.hpp>

uint16_t major_version = 0;
uint16_t minor_version = 0;

const signature::signature_t* signature::signature =
    reinterpret_cast<const signature::signature_t*>(lowLevel::InternalFlash::signature);
