from intelhex import IntelHex

Import('env')

def generate_arm(source, target, env, for_signature):
    return '$OBJCOPY --gap-fill 0xFF -O binary %s %s'%(source[0], target[0])
def generate_arm_srec(source, target, env, for_signature):
    return '$OBJCOPY -O srec %s %s'%(source[0], target[0])
def generate_arm_ihex(source, target, env, for_signature):
    return '$OBJCOPY -O ihex %s %s'%(source[0], target[0])
def srec_cat(source, target, env, for_signature):
    return '$SREC_CAT %s %s -Output %s'%(source[0], source[1], target[0])
def hex2bin(source, target, env, for_signature):
    return '$OBJCOPY --gap-fill 0xFF -O binary -I ihex %s %s'%(source[0], target[0])
def mergehex(source, target, env):
    ih = IntelHex()
    ih.padding = 0xFF
    for s in source:
        m = IntelHex(str(s))
        m.padding = 0xFF
        ih.merge(m, overlap='replace')
    ih.tofile(str(target[0]), format='hex')
    
env.Append(BUILDERS={
    'Objcopy': Builder(
               generator=generate_arm,
               suffix='.bin',
               src_suffix='.elf'),
    'hex2bin': Builder(
               generator=hex2bin,
               suffix='.bin',
               src_suffix='.hex'),
    'ObjcopySrec': Builder(
               generator=generate_arm_srec,
               suffix='.srec',
               src_suffix='.elf'),
    'ObjcopyIhex': Builder(
               generator=generate_arm_ihex,
               suffix='.hex',
               src_suffix='.srec'),
    'elf2hex': Builder(
               generator=generate_arm_ihex,
               suffix='.hex',
               src_suffix='.elf'),
    'mergehex': Builder(
               action=mergehex,
               suffix='.hex',
               src_suffix='.hex'),
    'SrecCat': Builder(
               generator=srec_cat,
               suffix='.srec',
               src_suffix='.srec')
    })

env['AR'] = 'arm-none-eabi-ar'
env['ASCOM'] = '$CC -x assembler-with-cpp $ASPPFLAGS $CFLAGS $CCFLAGS $SOURCES -c -o $TARGET'
env['CC'] = 'arm-none-eabi-gcc'
env['CXX'] = 'arm-none-eabi-g++'
env['RANLIB'] = 'arm-none-eabi-ranlib'
env['OBJCOPY'] = 'arm-none-eabi-objcopy'
env['SREC_CAT'] = 'srec_cat'

env['PROGSUFFIX'] = '.elf'

env['_LIBFLAGS'] = '-Wl,--whole-archive ${_stripixes(LIBLINKPREFIX, LIBS, LIBLINKSUFFIX, LIBPREFIXES, LIBSUFFIXES, __env__)} -Wl,--no-whole-archive'

env.Append(CCFLAGS = [
    '-fno-exceptions',
    '-fdata-sections',
    '-ffunction-sections',
    '-mcpu=cortex-m3',
    '-mthumb',
    '-gdwarf-2',
    ])

env.Append(LINKFLAGS = [
  '-mcpu=cortex-m3',
  '-mthumb',
  '-mfloat-abi=soft',
  '-specs=nano.specs',
  '-specs=nosys.specs',
  '-Wl,--gc-sections',
  '-lc',
  '-lm',
  '-lnosys',
    ])

env.BuildModules(env,env['MODULES'])
